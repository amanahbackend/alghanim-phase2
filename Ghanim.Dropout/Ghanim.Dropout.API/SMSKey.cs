﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Dropout.API
{
    public class SMSKey
    {
        public string URL { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
    }
}
