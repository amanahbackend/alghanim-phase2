﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
    public class TeamMember : BaseEntity, ITeamMember
    {
        public int? TeamId { get; set; }
        public int MemberParentId { get; set; }
        public string MemberParentName { get; set; }
        public int Rank { get; set; }
        public int MemberType { get; set; }
        public string PF { get; set; }
        public string MemberTypeName { get; set; }
        public string UserId { get; set; }
        public int? DivisionId { get; set; }
        public string DivisionName { get; set; }
        public bool IsDriver { get; set; }
    }
}
