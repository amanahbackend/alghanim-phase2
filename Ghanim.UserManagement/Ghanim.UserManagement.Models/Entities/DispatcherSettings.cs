﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
   public class DispatcherSettings : BaseEntity, IDispatcherSettings
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int OrderProblemId { get; set; }
        public string OrderProblemName { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int GroupId { get; set; }

    }
}
