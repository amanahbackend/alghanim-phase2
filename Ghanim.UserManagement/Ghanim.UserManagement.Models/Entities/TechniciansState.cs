﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
    public class TechniciansState : BaseEntity, ITechniciansState
    {
        public int TechnicianId { get; set; }
        public int TeamId { get; set; }
        public DateTime ActionDate { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public TechnicianState State { get; set; }

    }
}
