﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.UserManagement.Models.Entities
{
   public class Vehicle : BaseEntity, IVehicle
    {
        public string Plate_no { get; set; }
        public bool Isassigned { get; set; }
        public int TeamId { get; set; }
        [NotMapped]
        public Team Team { get; set; }
    }
}
