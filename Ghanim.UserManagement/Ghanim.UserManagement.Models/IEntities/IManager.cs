﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Ghanim.UserManagement.Models.IEntities
{
    public interface IManager : IBaseEntity
    {
        string UserId { get; set; }
        string Name { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        int CostCenterId { get; set; }
        string CostCenterName { get; set; }

    }
}
