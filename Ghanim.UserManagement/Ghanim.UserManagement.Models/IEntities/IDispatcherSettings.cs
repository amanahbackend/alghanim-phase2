﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
   public interface IDispatcherSettings : IBaseEntity
    {
        int DispatcherId { get; set; }
        string DispatcherName { get; set; }
        int OrderProblemId { get; set; }
        string OrderProblemName { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        int AreaId { get; set; }
        string AreaName { get; set; }
        int GroupId { get; set; }

    }
}
