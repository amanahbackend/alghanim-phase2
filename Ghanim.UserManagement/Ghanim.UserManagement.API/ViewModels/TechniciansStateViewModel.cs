﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class TechniciansStateViewModel : BaseEntity
    {
        public int TechnicianId { get; set; }
        public int TeamId { get; set; }
        public DateTime ActionDate { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public TechnicianState State { get; set; }
    }
}
