﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class VehicleViewModel :BaseEntity
    {
        
        public string Plate_no { get; set; }
        public bool Isassigned { get; set; }
        public int TeamId { get; set; }
       
    }
}
