﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class ForemanUserViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int EngineerId { get; set; }
        public string EngineerName { get; set; }
    }
}
