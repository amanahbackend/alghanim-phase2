﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class TeamMemberUserViewModel : BaseEntity
    {
        public int TeamId { get; set; }
        public int MemberParentId { get; set; }
        public string MemberParentName { get; set; }
        public int Rank { get; set; }
        public int MemberType { get; set; }
        public string MemberTypeName { get; set; }
        public string PF { get; set; }
        public string UserId { get; set; }
        public string CostCenter { get; set; }
        public int? CostCenterId { get; set; }
    }
}
