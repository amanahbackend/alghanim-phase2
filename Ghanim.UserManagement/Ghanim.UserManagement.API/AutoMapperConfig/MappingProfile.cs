﻿using AutoMapper;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace Ghanim.UserManagement.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Supervisor, SupervisorViewModel>()
           .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
           .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
           .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
           .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
           .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
           .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .IgnoreIdentityBaseEntityProperties();


            CreateMap<SupervisorViewModel, Supervisor>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Dispatcher, DispatcherViewModel>()
             .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
             .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
             .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
             .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
             .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
             .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
             .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName));

            CreateMap<DispatcherViewModel, Dispatcher>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Technician, TechnicianViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
               .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
               .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
               .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
               //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
               //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
               //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
               //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
               //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
               .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
               .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
               .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver));

            //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName));

            CreateMap<TechnicianViewModel, Technician>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
               .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Foreman, ForemanViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName));

            CreateMap<ForemanViewModel, Foreman>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Engineer, EngineerViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
              .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
              .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
              .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<EngineerViewModel, Engineer>()
                   .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                   .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                   .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                   .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                   .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                   .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                   .IgnoreIdentityBaseEntityProperties();

            CreateMap<Driver, DriverViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
               .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
               .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
               .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
               .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<DriverViewModel, Driver>()
                   .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                   .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                   .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                   .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                   .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                   .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                   .IgnoreIdentityBaseEntityProperties();

            CreateMap<Dispatcher, DispatcherUserViewModel>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName));

            CreateMap<DispatcherUserViewModel, Dispatcher>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, DispatcherUserViewModel>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                    .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<DispatcherUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<ApplicationUserViewModel, SupervisorUserViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<SupervisorUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<Supervisor, SupervisorUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<SupervisorUserViewModel, Supervisor>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, DriverUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<DriverUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<Driver, DriverUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.VehicleNo, opt => opt.MapFrom(src => src.VehicleNo))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<DriverUserViewModel, Driver>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.VehicleNo, opt => opt.MapFrom(src => src.VehicleNo))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, EngineerUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<EngineerUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<Engineer, EngineerUserViewModel>()
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<EngineerUserViewModel, Engineer>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, ForemanUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<ForemanUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<Foreman, ForemanUserViewModel>()
                     .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                     .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                     .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                     .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                     .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                     .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                     .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                     .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                     .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                     .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                     .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName));

            CreateMap<ForemanUserViewModel, Foreman>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                    .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, TechnicianUserViewModel>()
             .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
             .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
             .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<TechnicianUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            CreateMap<Technician, TechnicianUserViewModel>()
                     .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                     .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                     .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                     .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                     .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                     .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                     .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                     .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
                     .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver));
            //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
            //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
            //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
            //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
            //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
            //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName));

            CreateMap<TechnicianUserViewModel, Technician>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
                    .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver))
                    //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                    //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                    //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
                    //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName))
                    .IgnoreIdentityBaseEntityProperties();
            CreateMap<DispatcherSettings, DispatcherSettingsViewModel>();
            CreateMap<DispatcherSettingsViewModel, DispatcherSettings>()
                .IgnoreBaseEntityProperties();
            CreateMap<Vehicle, VehicleViewModel>();
            CreateMap<VehicleViewModel, Vehicle>()
                .IgnoreBaseEntityProperties();
            CreateMap<TeamMember, TeamMemberViewModel>();
            CreateMap<TeamMemberViewModel, TeamMember>();
            CreateMap<TeamMember, TeamMemberUserViewModel>();
            CreateMap<TeamMemberUserViewModel, TeamMember>();
            CreateMap<Team, TeamViewModel>();
            CreateMap<TeamViewModel, Team>();
            CreateMap<Manager, ManagerViewModel>();
            CreateMap<Manager, ManagerUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<ApplicationUserViewModel, ManagerUserViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
               .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
               .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
               .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
               .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ApplicationUserViewModel, MaterialControllerUserViewModel>()
       .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
       .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
       .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
       .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
       .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
       .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ManagerViewModel, Manager>();
            CreateMap<MaterialController, MaterialControllerViewModel>();
            CreateMap<MaterialController, MaterialControllerUserViewModel>();
            CreateMap<MaterialControllerViewModel, MaterialController>();
            CreateMap<TechniciansState, TechniciansStateViewModel>();
            CreateMap<TechniciansStateViewModel, TechniciansState>();

        }
    }
}