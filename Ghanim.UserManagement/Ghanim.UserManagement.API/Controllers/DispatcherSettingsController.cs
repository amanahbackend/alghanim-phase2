﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.UserManagement.API.Filters;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.API.ViewModels;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.BLL.NotMappedModels;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.RepositoryModule;
using Utilites.PaginatedItemsViewModel;
using Utilites.PaginatedItems;
using Utilites;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class DispatcherSettingsController : BaseController<IDispatcherSettingsManager, DispatcherSettings, DispatcherSettingsViewModel>
    {
        IDispatcherSettingsManager manager;
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public DispatcherSettingsController(IServiceProvider _serviceProvider, IDispatcherSettingsManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityUserService _identityUserService) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
        }
        private IDispatcherManager dispatcherManager
        {
            get
            {
                return serviceProvider.GetService<IDispatcherManager>();
            }
        }

        [HttpPost]
        [Route("AddWithAreaAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithAreaAndOrderProblems([FromBody]AreaWithOrderProblems Model)
        {
            try
            {
                int lastGroupId = manager.GetAll().Data.Max(x => x.GroupId);
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = lastGroupId++;
                foreach (var item in Model.OrderProblems)
                {
                    DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                    {
                        DispatcherId = Model.DispatcherId,
                        DispatcherName = Model.DispatcherName,
                        AreaId = Model.AreaId,
                        AreaName = Model.AreaName,
                        OrderProblemId = item.ProblemId,
                        OrderProblemName = item.ProblemName,
                        GroupId = lastGroupId
                    };
                    var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                    if (despatcherRes != null && despatcherRes.Data != null)
                    {
                        res.DivisionId = despatcherRes.Data.DivisionId;
                        res.DivisionName = despatcherRes.Data.DivisionName;
                    }
                    dispatcherSettingsRes.Add(res);
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("AddWithMultiAreaAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithMultiAreaAndOrderProblems([FromBody]List<AreaWithOrderProblems> lstModel)
        {
            try
            {
                int lastGroupId = manager.GetAll().Data.Max(x => x.GroupId);
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = lastGroupId++;
                foreach (var item in lstModel)
                {
                    foreach (var item2 in item.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = item.DispatcherId,
                            DispatcherName = item.DispatcherName,
                            AreaId = item.AreaId,
                            AreaName = item.AreaName,
                            OrderProblemId = item2.ProblemId,
                            OrderProblemName = item2.ProblemName,
                            GroupId = lastGroupId
                        };
                        var despatcherRes = dispatcherManager.Get(item.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("AddWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithAreasAndOrderProblems([FromBody]AreasWithOrderProblems Model)
        {
            try
            {
                int lastGroupId = 0;
                var AllRes = manager.GetAll();
                if (AllRes != null && AllRes.Data != null && AllRes.Data.Count > 0)
                {
                    lastGroupId = manager.GetAll().Data.Max(x => x.GroupId);
                }
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = ++lastGroupId;
                foreach (var area in Model.Areas)
                {
                    foreach (var OrderProblem in Model.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = Model.DispatcherId,
                            DispatcherName = Model.DispatcherName,
                            AreaId = area.AreaId,
                            AreaName = area.AreaName,
                            OrderProblemId = OrderProblem.ProblemId,
                            OrderProblemName = OrderProblem.ProblemName,
                            GroupId = lastGroupId
                        };
                        var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("AddWithMultiAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithMultiAreasAndOrderProblems([FromBody]List<AreasWithOrderProblems> lstModel)
        {
            try
            {
                int lastGroupId = 0;
                var AllRes = manager.GetAll();
                if (AllRes!=null && AllRes.Data !=null && AllRes.Data.Count >0)
                {
                    lastGroupId = manager.GetAll().Data.Max(x => x.GroupId);
                }
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                foreach (var item in lstModel)
                {
                    lastGroupId = ++lastGroupId;
                    foreach (var area in item.Areas)
                    {
                        foreach (var OrderProblem in item.OrderProblems)
                        {
                            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                            {
                                DispatcherId = item.DispatcherId,
                                DispatcherName = item.DispatcherName,
                                AreaId = area.AreaId,
                                AreaName = area.AreaName,
                                OrderProblemId = OrderProblem.ProblemId,
                                OrderProblemName = OrderProblem.ProblemName,
                                GroupId = lastGroupId
                            };
                            var despatcherRes = dispatcherManager.Get(item.DispatcherId);
                            if (despatcherRes != null && despatcherRes.Data != null)
                            {
                                res.DivisionId = despatcherRes.Data.DivisionId;
                                res.DivisionName = despatcherRes.Data.DivisionName;
                            }
                            dispatcherSettingsRes.Add(res);
                        }
                    }
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpGet]
        [Route("GetbyDispatcherId/{dispatcherId}")]
        public List<AreaWithOrderProblems> GetbyDispatcherId([FromRoute]int dispatcherId)
        {
            try
            {
                var entityResult = manager.GetByDispatcherId(dispatcherId);
                List<AreaWithOrderProblems> areaWithOrderProblems = new List<AreaWithOrderProblems>();
                AreaWithOrderProblems areaWithOrderProblem = new AreaWithOrderProblems();
                List<Problems> problems = new List<Problems>();
                if (entityResult != null)
                {
                    int i = 0;
                    foreach (var item in entityResult.Data)
                    {
                        if (i > 0 && areaWithOrderProblems[i - 1].AreaId == item.AreaId)
                        {
                            Problems problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblemName;
                            problems.Add(problem);
                            areaWithOrderProblem.OrderProblems = problems;
                        }
                        else
                        {
                            areaWithOrderProblem = new AreaWithOrderProblems();
                            areaWithOrderProblem.DispatcherId = item.DispatcherId;
                            areaWithOrderProblem.DispatcherName = item.DispatcherName;
                            areaWithOrderProblem.AreaId = item.AreaId;
                            areaWithOrderProblem.AreaName = item.AreaName;
                            Problems problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblemName;
                            problems = new List<Problems>();
                            problems.Add(problem);
                            areaWithOrderProblem.OrderProblems = problems;
                            areaWithOrderProblems.Add(areaWithOrderProblem);
                            i++;
                        }

                    }

                }
                return areaWithOrderProblems;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        [HttpGet]
        [Route("GetMultiAreasAndProblemsbyDispatcherId/{dispatcherId}")]
        public List<AreasWithOrderProblems> GetMultiAreasAndProblemsbyDispatcherId([FromRoute]int dispatcherId)
        {
            try
            {
                var entityResult = manager.GetByDispatcherId(dispatcherId);
                List<AreasWithOrderProblems> areasWithOrderProblemsLST = new List<AreasWithOrderProblems>();
                // get grouped data
                var grpRes = entityResult.Data.GroupBy(x => x.GroupId).Select(grp => grp.ToList()).ToList();
                // customize objects
                AreasWithOrderProblems areasWithOrderProblems = new AreasWithOrderProblems();
                List<Problems> problems ;
                List<Areas> areas;
                Problems problem ;
                Areas area ;
                List<int> AreasId;
                List<int> problemsId;

                if (grpRes != null && grpRes != null&& grpRes.Count > 0)
                {
                    foreach (var item2 in grpRes)
                    {
                        areasWithOrderProblems = new AreasWithOrderProblems();
                        problemsId = new List<int>();
                        AreasId = new List<int>();
                        AreasId = new List<int>();
                        areas = new List<Areas>();
                        problems = new List<Problems>();
                        foreach (var item in item2)
                        {
                            
                            if (!AreasId.Contains(item.AreaId))
                            {
                                area = new Areas();
                                area.AreaId = item.AreaId;
                                area.AreaName = item.AreaName;
                                AreasId.Add(item.AreaId);
                                areas.Add(area);
                            }
                            if (!problemsId.Contains(item.OrderProblemId))
                            {
                                problem = new Problems();
                                problem.ProblemId = item.OrderProblemId;
                                problem.ProblemName = item.OrderProblemName;
                                problemsId.Add(item.OrderProblemId);
                                problems.Add(problem);
                            }
                        }
                        areasWithOrderProblems.DispatcherId = item2.FirstOrDefault().DispatcherId;
                        areasWithOrderProblems.DispatcherName = item2.FirstOrDefault().DispatcherName;
                        areasWithOrderProblems.OrderProblems = problems;
                        areasWithOrderProblems.Areas = areas;
                        areasWithOrderProblems.GroupId = item2.FirstOrDefault().GroupId;
                        areasWithOrderProblemsLST.Add(areasWithOrderProblems);
                    }
                    
                }

                return areasWithOrderProblemsLST;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("GetDispatcherBySettings")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> GetDispatcherBySettings([FromBody]SettingsViewModel settings)
        {
            try
            {
                var dispatcherSettings = manager.GetAll().Data.Where(x => x.DivisionId == settings.DivisionId && x.AreaId == settings.AreaId && x.OrderProblemId == settings.ProblemId).ToList();
                if (dispatcherSettings != null && dispatcherSettings.Count > 0)
                {
                    List<DispatcherSettingsViewModel> result = mapper.Map<List<DispatcherSettings>, List<DispatcherSettingsViewModel>>(dispatcherSettings);
                    return ProcessResultViewModelHelper.Succedded<List<DispatcherSettingsViewModel>>(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<List<DispatcherSettingsViewModel>>(null, "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpGet]
        [Route("GetbyGroupId/{GroupId}")]
        public AreasWithOrderProblems GetbyGroupId([FromRoute]int GroupId)
        {
            try
            {
                var entityResult = manager.GetbyGroupId(GroupId);
                AreasWithOrderProblems areasWithOrderProblem = new AreasWithOrderProblems();
                List<Problems> problems = new List<Problems>();
                List<Areas> areas = new List<Areas>();
                Problems problem = new Problems();
                Areas area = new Areas();
                List<int> AreasId = new List<int>();
                List<int> problemsId = new List<int>();
                if (entityResult != null && entityResult.Data !=null)
                {
                    
                    foreach (var item in entityResult.Data)
                    {
                        
                        if (!AreasId.Contains(item.AreaId))
                        {
                            area = new Areas();
                            area.AreaId = item.AreaId;
                            area.AreaName = item.AreaName;
                            AreasId.Add(item.AreaId);
                            areas.Add(area);
                        }
                        if (!problemsId.Contains(item.OrderProblemId))
                        {
                            problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblemName;
                            problemsId.Add(item.OrderProblemId);
                            problems.Add(problem);
                        }                                                
                    }
                    areasWithOrderProblem.DispatcherId = entityResult.Data.FirstOrDefault().DispatcherId;
                    areasWithOrderProblem.DispatcherName = entityResult.Data.FirstOrDefault().DispatcherName;
                    areasWithOrderProblem.OrderProblems = problems;
                    areasWithOrderProblem.Areas = areas;
                    areasWithOrderProblem.GroupId = GroupId;
                }
                return areasWithOrderProblem;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("UpdateWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> UpdateWithAreasAndOrderProblems([FromBody]AreasWithOrderProblems Model)
        {
            try
            {
                // delete first
                var entityResult = manager.GetbyGroupId(Model.GroupId);
                if (entityResult != null && entityResult.Data != null) {
                    foreach (var item in entityResult.Data)
                    {
                        var deleteRes = base.Delete(item.Id);
                    }
                   
                }
                // add new dispatcher Settings
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                foreach (var area in Model.Areas)
                {
                    foreach (var OrderProblem in Model.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = Model.DispatcherId,
                            DispatcherName = Model.DispatcherName,
                            AreaId = area.AreaId,
                            AreaName = area.AreaName,
                            OrderProblemId = OrderProblem.ProblemId,
                            OrderProblemName = OrderProblem.ProblemName,
                            GroupId = Model.GroupId
                        };
                        var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("UpdateMulitWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> UpdateMulitWithAreasAndOrderProblems([FromBody]List<AreasWithOrderProblems> lstModel)
        {
            try
            {
                ProcessResultViewModel<List<DispatcherSettingsViewModel>> Result = new ProcessResultViewModel<List<DispatcherSettingsViewModel>> { Data = null };
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                foreach (var model in lstModel)
                {
                    // delete first
                    var entityResult = manager.GetbyGroupId(model.GroupId);
                    if (entityResult != null && entityResult.Data != null)
                    {
                        foreach (var item in entityResult.Data)
                        {
                            var deleteRes = base.Delete(item.Id);
                        }
                    }
                    // add new dispatcher Settings
                    
                    foreach (var area in model.Areas)
                    {
                        foreach (var OrderProblem in model.OrderProblems)
                        {
                            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                            {
                                DispatcherId = model.DispatcherId,
                                DispatcherName = model.DispatcherName,
                                AreaId = area.AreaId,
                                AreaName = area.AreaName,
                                OrderProblemId = OrderProblem.ProblemId,
                                OrderProblemName = OrderProblem.ProblemName,
                                GroupId = model.GroupId
                            };
                            var despatcherRes = dispatcherManager.Get(model.DispatcherId);
                            if (despatcherRes != null && despatcherRes.Data != null)
                            {
                                res.DivisionId = despatcherRes.Data.DivisionId;
                                res.DivisionName = despatcherRes.Data.DivisionName;
                            }
                            dispatcherSettingsRes.Add(res);
                        }
                    }
                }
                Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpPost]
        [Route("Search")]
        public async Task<IProcessResultViewModel<PaginatedItemsViewModel<DispatcherSettingsViewModel>>> Search([FromBody]DispatcherSettingsFiltersViewModel model)
        {
            try {

                var action = mapper.Map<PaginatedItemsViewModel<DispatcherSettingsViewModel>, PaginatedItems<DispatcherSettings>>(model.PageInfo);
                var predicate = PredicateBuilder.True<DispatcherSettings>();

                if (model.AreaNames.Count>0)
                {
                    predicate = predicate.And(r=> model.AreaNames.Contains(r.AreaName));
                }
                if (model.OrderProblemNames.Count > 0)
                {
                   predicate = predicate.And(r => model.OrderProblemNames.Contains(r.OrderProblemName));
                }
                if (model.DispatcherNames.Count > 0)
                {
                   predicate = predicate.And(r => model.DispatcherNames.Contains(r.DispatcherName));
                }

                  var paginatedRes = manager.GetAllPaginated(action,predicate);
                  var result = processResultMapper.Map<PaginatedItems<DispatcherSettings>, PaginatedItemsViewModel<DispatcherSettingsViewModel>>(paginatedRes);
                  return result;
            }
            catch (Exception ex)  
            {
                    return ProcessResultViewModelHelper.Failed<PaginatedItemsViewModel<DispatcherSettingsViewModel>>(null, "Something wrong while getting data from Dispatcher Settings manager");
            }
        }

        [HttpGet]
        [Route("GetAllDispatchers")]
        public List<DispatcherViewModel> GetAllDispatchers()
        {
            try
            {
                List<DispatcherViewModel> Dispatchers = new List<DispatcherViewModel>();
                Dispatchers = manager.GetAllQuerable().Data.Select(x => new DispatcherViewModel { Id = x.DispatcherId, Name = x.DispatcherName }).DistinctBy(x => x.Name).ToList();
                return Dispatchers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
