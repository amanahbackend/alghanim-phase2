﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.UserManagement.API.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.UserManagement.API.ServiceCommunications.Identity;
using Ghanim.UserManagement.BLL.IManagers;

namespace Ghanim.UserManagement.API.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class UserController : Controller
    {
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityUserService identityUserService;
        IDispatcherManager dispatcherManager;
        ISupervisorManager supervisorManager;
        ITechnicianManager technicianManager;
        ITeamMemberManager teamMemberManager;
        public UserController(IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService _identityUserService,
            IDispatcherManager _dispatcherManager,
            ISupervisorManager _supervisorManager,
            ITechnicianManager _technicianManager,
            ITeamMemberManager _teamMemberManager)
        {
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            dispatcherManager = _dispatcherManager;
            supervisorManager = _supervisorManager;
            technicianManager = _technicianManager;
            teamMemberManager = _teamMemberManager;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody] LoginViewModel model)
        {

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Username))
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "username & password are required");
            }
            else
            {
                ProcessResultViewModel<LoginResultViewModel> loginResult = await identityUserService.Login(model);

                if (loginResult.IsSucceeded && loginResult.Data != null)
                {
                    if (loginResult.Data.Roles.Contains("Dispatcher"))
                    {
                        var dispatcherResult = dispatcherManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (dispatcherResult.IsSucceeded && dispatcherResult.Data != null)
                        {
                            loginResult.Data.Id = dispatcherResult.Data.Id;
                            return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching dispatcher data");
                        }
                    }
                    else if (loginResult.Data.Roles.Contains("Supervisor"))
                    {
                        var supervisorResult = supervisorManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (supervisorResult.IsSucceeded && supervisorResult.Data != null)
                        {
                            loginResult.Data.Id = supervisorResult.Data.Id;
                            return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching supervisor data");
                        }
                    }
                    else if (loginResult.Data.Roles.Contains("Technician"))
                    {
                        var technicianResult = technicianManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (technicianResult.IsSucceeded && technicianResult.Data != null)
                        {
                            var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);
                            if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                            {
                                if (teamMemberResult.Data.TeamId != null)
                                {
                                    loginResult.Data.TeamId = (int)teamMemberResult.Data.TeamId;
                                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                                }
                                else
                                {
                                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data, "this technican is not assigned to team");
                                }
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching technician's member user");
                            }
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching technician data");
                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                    }
                }
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while login through identity service");
            }
        }
    }
}