﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ServiceCommunications.Identity
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string ServiceName { get; set; }
        public string GetUserById { get; set; }
        public string GetByIdsAction { get; set; }
        public string SearchAction { get; set; }
        public string ResetPassword { get; set; }
        public string Login { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
