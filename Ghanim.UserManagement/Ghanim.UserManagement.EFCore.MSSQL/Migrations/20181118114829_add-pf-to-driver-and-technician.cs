﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class addpftodriverandtechnician : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Technicians",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Drivers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PF",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Drivers");
        }
    }
}
