﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.EFCore.MSSQL.EntitiesConfiguration
{
    class EngineerTypeConfiguration : BaseEntityTypeConfiguration<Engineer>
    {
        public void Configure(EntityTypeBuilder<Engineer> builder)
        {
            base.Configure(builder);
            builder.ToTable("Engineer");
        }
    }
}