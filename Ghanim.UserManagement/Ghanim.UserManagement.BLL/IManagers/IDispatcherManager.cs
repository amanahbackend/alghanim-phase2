﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface IDispatcherManager : IRepository<Dispatcher>
    {
        ProcessResult<Dispatcher> GetByUserId(string userId);
       
    }
}