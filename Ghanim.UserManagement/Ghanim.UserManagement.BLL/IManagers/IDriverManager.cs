﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface IDriverManager : IRepository<Driver>
    {
    }
}
