﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.Models.Entities;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface IManagerManager :IRepository<Manager>
    {
       ProcessResult<Manager> GetByUserId(string userId);
    }
}
