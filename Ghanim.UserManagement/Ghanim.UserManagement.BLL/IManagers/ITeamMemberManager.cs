﻿using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Ghanim.UserManagement.BLL.NotMappedModels;
namespace Ghanim.UserManagement.BLL.IManagers
{
    public interface ITeamMemberManager : IRepository<TeamMember>
    {
        ProcessResult<List<TeamMember>> GetUnassignedMember();
        ProcessResult<List<TeamMember>> GetUnassignedMemberByDivisionId(int divisionId);
        ProcessResult<bool> AssignedMemberToTeam(int teamId,int MemberParentId);
        ProcessResult<List<TeamMember>> GetUnassignedVehicleMember();
        ProcessResult<TeamMember> GetByMemberType_TeamId(int teamId, int memberType);
        ProcessResult<TeamMember> GetByMemberParentId(int memberParentId);
        ProcessResult<List<TeamMember>> GetByTeamId(int teamId);
        ProcessResult<bool> SortMemeber(int teamId, List<TeamMemberRanks> teamMembers);
        ProcessResult<List<TeamMember>> SearchVec(string word);
        ProcessResult<List<TeamMember>> SearchTech(string word);
    }
}