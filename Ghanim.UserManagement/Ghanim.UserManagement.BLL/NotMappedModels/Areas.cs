﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.NotMappedModels
{
    public class Areas
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
