﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class AttendenceManager : Repository<Attendence>, IAttenenceManager
    {
        IServiceProvider serviceProvider;
        public AttendenceManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
    }
}