﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
   public class SupervisorManager : Repository<Supervisor>, ISupervisorManager
    {
        IServiceProvider serviceProvider;
        public SupervisorManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Supervisor> GetByUserId(string userId)
        {
            Supervisor input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succedded<Supervisor>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Supervisor>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
    }
}