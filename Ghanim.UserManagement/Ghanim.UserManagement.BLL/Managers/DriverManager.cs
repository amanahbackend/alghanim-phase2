﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.Managers
{
   public class DriverManager : Repository<Driver>, IDriverManager
    {
        IServiceProvider serviceProvider;
        public DriverManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
    }
}