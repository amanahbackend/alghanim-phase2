﻿using DispatchProduct.RepositoryModule;
using Ghanim.UserManagement.BLL.IManagers;
using Ghanim.UserManagement.EFCore.MSSQL.Context;
using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.UserManagement.BLL.Managers
{
    public class ForemanManager : Repository<Foreman>, IForemanManager
    {
        IServiceProvider serviceProvider;
        public ForemanManager(IServiceProvider _serviceProvider, UserManagementContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Foreman> GetByUserId(string userId)
        {
            Foreman input = null;
            try
            {
                input = GetAll().Data.Find(x => x.UserId == userId);
                return ProcessResultHelper.Succedded<Foreman>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Foreman>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
    }
}