﻿using Ghanim.Identity.API.Schedulers.TaskFlow;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.Schedulers
{
    public class LogoutTask : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;
        private LogoutFlow _logoutFlow;
        public LogoutTask(ILogger<LogoutTask> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                _logoutFlow = scope.ServiceProvider.GetRequiredService<LogoutFlow>();
            }
        }           

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Logout Background Service is starting.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(28800));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Logout Background Service is working.");
            _logoutFlow.LogoutService();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Logout Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
