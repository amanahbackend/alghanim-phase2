﻿using DispatchProduct.RepositoryModule;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.BLL.Managers;
using Ghanim.Identity.Models.Context;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using IdentityServer4.Services;
using Ghanim.Identity.API.Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using DnsClient;
using System.Linq;
using System.Net.Sockets;
using BuildingBlocks.ServiceDiscovery;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Ghanim.Identity.API.AutoMapperConfig;
using Ghanim.Identity.API.Schedulers.TaskFlow;
using Ghanim.Identity.API.Schedulers;
using Ghanim.Order.API;

namespace Ghanim.Identity.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            StaticAppSettings.ConnectionString = Configuration.GetSection("ConnectionString").Get<string>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.Identity.EFCore.MSSQL")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(opt =>
            {
                opt.User.RequireUniqueEmail = true;
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 1;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAutoMapper(typeof(Startup));
            //Mapper.AssertConfigurationIsValid();

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            //services.AddIdentityServer();
            //services.Configure<VehicleAppSettings>(Configuration);

            services.AddScoped<DbContext, ApplicationDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));

            services.AddScoped(typeof(ApplicationRole), typeof(ApplicationRole));
            services.AddScoped(typeof(PasswordTokenPin), typeof(PasswordTokenPin));

            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(IApplicationUserHistoryManager), typeof(ApplicationUserHistoryManager));
            services.AddScoped(typeof(IPasswordTokenPinManager), typeof(PasswordTokenPinManager));
            services.AddScoped(typeof(IJunkUserManager), typeof(JunkUserManager));
            services.AddScoped(typeof(IPrivilegeManager), typeof(PrivilegeManager));
            services.AddScoped(typeof(IRolePrivilegeManager), typeof(RolePrivilegeManager));
            services.AddScoped(typeof(IUserDeviceManager), typeof(UserDeviceManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(IUploadImageFileManager), typeof(UploadImageFileManager));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<LogoutFlow>();
            services.AddHostedService<LogoutTask>();
            services.AddDbContext<ApplicationDbContext>
      (options => options.UseSqlServer(Configuration["ConnectionString"]));

            services.AddIdentityServer()
               .AddDeveloperSigningCredential()
               // this adds the config data from DB (clients, resources)
               .AddAspNetIdentity<ApplicationUser>()
               .AddConfigurationStore(options =>
               {
                   options.ConfigureDbContext = builder =>
                       builder.UseSqlServer(Configuration["ConnectionString"],
                           sql => sql.MigrationsAssembly("Ghanim.Identity.EFCore.MSSQL"));
               })
               // this adds the operational data from DB (codes, tokens, consents)
               .AddOperationalStore(options =>
               {
                   options.ConfigureDbContext = builder =>
                       builder.UseSqlServer(Configuration["ConnectionString"],
                           sql => sql.MigrationsAssembly("Ghanim.Identity.EFCore.MSSQL"));

                   // this enables automatic token cleanup. this is optional.
                   options.EnableTokenCleanup = true;
                   options.TokenCleanupInterval = 3600;
               })
               .Services.AddTransient<IProfileService, IdentityProfileService>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    //ValidIssuer = Configuration["JwtSecurityToken:Issuer"],
                    //ValidAudience = Configuration["JwtSecurityToken:Audience"],
                    ValidateIssuerSigningKey = true,
                    //IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityToken:Key"])),
                    ValidateLifetime = true
                };
            });


            // Dns Query for consul - service discovery
            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            //services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Identity API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                // Assign scope requirements to operations based on AuthorizeAttribute
                // options.OperationFilter<SecurityRequirementsOperationFilter>();
            });
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            //        services.AddMvcCore()
            //.AddAuthorization() // Note - this is on the IMvcBuilder, not the service collection
            //.AddJsonFormatters(options => options.ContractResolver = new CamelCasePropertyNamesContractResolver());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseStaticFiles();
            // Autoregister using server.Features (does not work in reverse proxy mode)
            app.UseConsulRegisterService();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Identity API V1");
            }); // URL: /swagger
        }
    }
}
