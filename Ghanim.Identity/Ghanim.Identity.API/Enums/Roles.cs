﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.Enums
{
        public enum Roles
        {
        Superadmin = 1,
        Production = 2 ,
        Driver=3,
        CustomerService=4,
        Guest=5,
        Customer=6,
        Logistics=7,
        Accountant=8,
        Fleet=9,
        Admin=10,
        Sales=11,
    }
}
