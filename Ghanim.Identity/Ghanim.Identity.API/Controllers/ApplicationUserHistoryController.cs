﻿using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.Identity.API.ViewModels;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Identity.API.Controllers
{
    [Route("api/[controller]")]
    public class ApplicationUserHistoryController : BaseController<IApplicationUserHistoryManager, ApplicationUserHistory, ApplicationUserHistoryViewModel>
    {
        IApplicationUserHistoryManager manager;
        IProcessResultMapper processResultMapper;
        public ApplicationUserHistoryController(IApplicationUserHistoryManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        [HttpGet, Route("IsTokenExpired")]
        public ProcessResultViewModel<bool> IsTokenExpired([FromQuery]string token)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var applicationUserHistoryRes = manager.IsTokenExpired(token);
                if (applicationUserHistoryRes != null && applicationUserHistoryRes.Data != null && applicationUserHistoryRes.Data.Count > 0)
                {
                    result = ProcessResultViewModelHelper.Succedded(true);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Succedded(false);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }
    }
}
