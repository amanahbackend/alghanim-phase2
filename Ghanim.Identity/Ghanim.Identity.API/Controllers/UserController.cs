﻿using AutoMapper;
using Ghanim.Identity.API.Utilities;
using Ghanim.Identity.API.ViewModels;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilites;
using System.IO;
using Ghanim.Identity.API.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.Identity.API.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IPasswordTokenPinManager _passwordTokenPinManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IUploadImageFileManager _imageManager;
        private readonly IUserDeviceManager _userDeviceManager;
        IServiceProvider _serviceprovider;

        public UserController(IServiceProvider serviceprovider, IApplicationUserManager applicationUserManager,
            IHostingEnvironment hostingEnv, IConfigurationRoot configuration,
            IPasswordTokenPinManager passwordTokenPinManager, IPasswordHasher<ApplicationUser> passwordHasher,
             IUploadImageFileManager imageManager,
             IProcessResultMapper processResultMapper,
             IUserDeviceManager userDeviceManager
             )
        {
            _serviceprovider = serviceprovider;
            _passwordHasher = passwordHasher;
            _passwordTokenPinManager = passwordTokenPinManager;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _applicationUserManager = applicationUserManager;
            _imageManager = imageManager;
            _userDeviceManager = userDeviceManager;
            _processResultMapper = processResultMapper;
        }

        private IApplicationUserHistoryManager ApplicationUserHistorymanager
        {
            get
            {
                return _serviceprovider.GetService<IApplicationUserHistoryManager>();
            }
        }

        [HttpPost, Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody]LoginViewModel loginViewModel)
        {
            try
            {
                var response = await TokenManager.GetToken(loginViewModel, _applicationUserManager, _passwordHasher, _userDeviceManager);
                if (response != null && response.Item2 != null && response.Item1 != null)
                {
                    LoginResultViewModel loginResult = new LoginResultViewModel()
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(response.Item2),
                        UserName = response.Item1.UserName,
                        Name = response.Item1.FirstName + " " + response.Item1.LastName,
                        Roles = response.Item1.RoleNames,
                        userId = response.Item1.Id
                    };
                    ApplicationUserHistoryViewModel ApplicationUserHistoryRes = new ApplicationUserHistoryViewModel()
                    {
                        Token = loginResult.Token,
                        LoginDate = DateTime.UtcNow,
                        UserId = response.Item1.Id,
                        UserType = loginResult.Roles.FirstOrDefault(),
                        DeveiceId=loginViewModel.DeviceId
                    };

                    ApplicationUserHistory userHistory = Mapper.Map<ApplicationUserHistoryViewModel, ApplicationUserHistory>(ApplicationUserHistoryRes);
                    var ApplicationUserHistoryResult = ApplicationUserHistorymanager.Add(userHistory);

                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "Username or password is invalid");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, ex.Message);
            }
        }

        [HttpPost, Route("AddUserDevice")]
        public async Task<ProcessResultViewModel<bool>> AddUserDevice([FromBody]UserDevice userDevice)
        {
            _userDeviceManager.AddIfNotExist(userDevice);
            return ProcessResultViewModelHelper.Succedded(true);
        }

        [HttpPut, Route("Logout")]
        public  ProcessResultViewModel<bool> Update([FromQuery]string userId, [FromQuery] string deviceId)
        {

            var loginUserRes = ApplicationUserHistorymanager.GetLoginUser(userId,deviceId);
            if (loginUserRes !=null && loginUserRes.Data.Count >0)
            {
                foreach (var item in loginUserRes.Data)
                {
                    item.LogoutDate = DateTime.UtcNow;
                    var logoutUserRes = ApplicationUserHistorymanager.Update(item);
                }
                 _userDeviceManager.Delete(deviceId);
                return ProcessResultViewModelHelper.Succedded(true);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "This User can't logout. ");
            }

        }

        [HttpGet, Route("Test")]
        public IActionResult Test()
        {
            //var currentUserName = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var currentUserName2 = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sub).Value;
            var currentUserId = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Jti).Value;
            return Ok();
        }

        [HttpPost, Route("Add")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Add([FromBody] ApplicationUserViewModel userViewModel)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                //if (userViewModel.Picture != null)
                //{
                //    userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                //                                                 userViewModel.UserName, _imageManager, _hostingEnv);
                //}
                ApplicationUser user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);

                var userAddResult = await _applicationUserManager.AddUserAsync(user, userViewModel.Password);
                if (userAddResult.IsSucceeded)
                {
                    user = userAddResult.Data;
                    userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    result = ProcessResultViewModelHelper.Succedded(userViewModel);
                }
                else
                {
                    //if (userViewModel.Picture != null)
                    //{
                    //    ProfilePictureManager.DeleteProfilePicture(user.UserName, _hostingEnv);
                    //    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, userAddResult.Exception.Message);
                    //}
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, userAddResult.Exception.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetBy")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetBy([FromQuery] string username)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                ApplicationUser user = await _applicationUserManager.GetBy(username);
                if (user != null)
                {
                    //user.Picture = ProfilePictureManager.GetProfilePictureBase64(username, _hostingEnv);
                    var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);

                    result = ProcessResultViewModelHelper.Succedded(userModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User not found");
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Update([FromBody]ApplicationUserViewModel userViewModel)
        {
            if (userViewModel.Picture != null)
            {
                userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                                                                 userViewModel.UserName, _imageManager, _hostingEnv);
            }
            var user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);
            var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
            if (updateUserResult.IsSucceeded)
            {
                user = updateUserResult.Data;
                userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, updateUserResult.Exception.Message);
            }

        }

        [HttpGet, Route("GetAll")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAll()
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetAll();
                if (users.Count > 0)
                {
                    List<ApplicationUserViewModel> userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }
        //[HttpPost, Route("GetCustomerUsersPaginatedByCountry/{country}")]
        //public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCustomerUsersPaginatedByCountry
        //    ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        //{
        //    var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
        //    {
        //        Count = model.Count,
        //        PageNo = model.PageNo,
        //        PageSize = model.PageSize
        //    };
        //    var resultData = await _applicationUserManager.GetAllCustomersPaginated(paginatedItems, country);
        //    model.Count = resultData.Count;
        //    model.PageNo = resultData.PageNo;
        //    model.PageSize = resultData.PageSize;
        //    model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
        //    return model;
        //}

        //[HttpPost, Route("GetCompanyUsersPaginatedByCountry/{country}")]
        //public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCompanyUsersPaginatedByCountry
        //    ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        //{
        //    var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
        //    {
        //        Count = model.Count,
        //        PageNo = model.PageNo,
        //        PageSize = model.PageSize
        //    };
        //    var resultData = await _applicationUserManager.GetAllCompaniesPaginated(paginatedItems, country);
        //    model.Count = resultData.Count;
        //    model.PageNo = resultData.PageNo;
        //    model.PageSize = resultData.PageSize;
        //    model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
        //    return model;
        //}

        [HttpDelete, Route("Delete")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _applicationUserManager.DeleteAsync(username);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("DeleteById")]
        public async Task<ProcessResultViewModel<bool>> DeleteById([FromQuery] string id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(id);
                var username = user?.UserName;
                var isDeleted = await _applicationUserManager.DeleteByIdAsunc(id);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByRole")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByRole([FromQuery] string role)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = (List<ApplicationUser>)await _applicationUserManager.GetUsersInRole(role);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users with this role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAdminUsers")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAdminUsers()
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = (List<ApplicationUser>)await _applicationUserManager.GetUsersInRole(Enum.GetName(typeof(Roles),Roles.Admin));
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users with this role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsEmailExist")]
        public async Task<ProcessResultViewModel<bool>> IsEmailExist([FromQuery]string email)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isEmailExist = await _applicationUserManager.IsEmailExistAsync(email);
                result = ProcessResultViewModelHelper.Succedded(isEmailExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsUsernameExist")]
        public async Task<ProcessResultViewModel<bool>> IsUsernameExist([FromQuery]string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExisting = await _applicationUserManager.IsUserNameExistAsync(username);
                result = ProcessResultViewModelHelper.Succedded(isExisting);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsPhoneExist")]
        public ProcessResultViewModel<bool> IsPhoneExist([FromQuery] string phone)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isPhoneExist = _applicationUserManager.IsPhoneExist(phone);
                result = ProcessResultViewModelHelper.Succedded(isPhoneExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Search")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> Search([FromQuery] string searchToken)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                string[] searchFields = _configuration.GetSection("UserSearchFields").Get<string[]>();
                var users = await _applicationUserManager.Search(searchToken, searchFields);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Succedded<List<ApplicationUserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Get")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Get([FromQuery]string id)
        {
            var user = await _applicationUserManager.Get(id);
            if (user != null)
            {
                var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);
                return ProcessResultViewModelHelper.Succedded(userModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User can not be found");
            }
        }

        [HttpGet, Route("Deactivate")]
        public async Task<ProcessResultViewModel<bool>> Deactivate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeactivated = await _applicationUserManager.Deactivate(username);
                result = ProcessResultViewModelHelper.Succedded(isDeactivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Activate")]
        public async Task<ProcessResultViewModel<bool>> Activate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isActivated = await _applicationUserManager.Activate(username);
                result = ProcessResultViewModelHelper.Succedded(isActivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        //[HttpPost, Route("GetPhoneVerficationToken")]
        //public async Task<ProcessResultViewModel<string>> GetPhoneVerficationToken([FromBody] PhoneValidationViewModel model)
        //{
        //    ProcessResultViewModel<string> result = null;
        //    try
        //    {
        //        var code = await _applicationUserManager.GeneratePhoneNumberToken(model.Username, model.Phone);
        //        SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
        //        {
        //            Message = $"Mawashi verification code: {code}",
        //            PhoneNumber = model.Phone
        //        };
        //        if (model.Country == Country.Kuwait)
        //        {
        //            _dropoutService.SendKw("v1", sendSmsViewModel);
        //        }
        //        else if (model.Country == Country.UAE)
        //        {
        //            _dropoutService.SendUae("v1", sendSmsViewModel);
        //        }
        //        var user = await _applicationUserManager.GetBy(model.Username);
        //        _dropoutService.SendMail("v1", new SendMailViewModel
        //        {
        //            Subject = "Mawashi: Activate Your Account",
        //            To = new List<string> { user.Email },
        //            Body = $"<h2>Welcome {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi verification code: {code}</h4>"
        //        });
        //        result = ProcessResultViewModelHelper.Succedded(code);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
        //    }
        //    return result;
        //}

        [HttpPost, Route("IsPhoneVerificationTokenValid")]
        public async Task<ProcessResultViewModel<bool>> IsPhoneVerificationTokenValid([FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isValid = await _applicationUserManager.CheckPhoneValidationToken(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ConfirmPhone")]
        public async Task<ProcessResultViewModel<bool>> ConfirmPhone([FromBody]PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var confirmed = await _applicationUserManager.ConfirmPhone(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(confirmed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        //[HttpGet, Route("GenerateForgetPasswordToken")]
        //public async Task<ProcessResultViewModel<string>> GenerateForgetPasswordToken([FromQuery]string username)
        //{
        //    ProcessResultViewModel<string> result = null;
        //    try
        //    {
        //        var token = await _applicationUserManager.GenerateForgetPasswordToken(username);
        //        // we will implement send sms with this token to the user phone
        //        var user = await _applicationUserManager.GetBy(username);

        //        SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
        //        {
        //            Message = $"Mawashi forget password verification code: {token}",
        //            PhoneNumber = user.Phone1
        //        };
        //        if (user.Fk_Country_Id == Country.Kuwait)
        //        {
        //            _dropoutService.SendKw("v1", sendSmsViewModel);
        //        }
        //        else if (user.Fk_Country_Id == Country.UAE)
        //        {
        //            _dropoutService.SendUae("v1", sendSmsViewModel);
        //        }
        //        _dropoutService.SendMail("v1", new SendMailViewModel
        //        {
        //            Subject = "Mawashi: Forget Password",
        //            To = new List<string> { user.Email },
        //            Body = $"<h2>Hello {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi forget password verification code: {token}</h4>"
        //        });

        //        result = ProcessResultViewModelHelper.Succedded(token);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
        //    }
        //    return result;
        //}

        [HttpPost, Route("ForgetPassword")]
        public async Task<ProcessResultViewModel<bool>> ForgetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ForgetPassword(model.Username, model.NewPassword, model.Code);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ChangePassword")]
        public async Task<ProcessResultViewModel<bool>> ChangePassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ChangePassword(model.Username, model.OldPassword, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ResetPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isReset = await _applicationUserManager.ResetPassword(model.Username, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(isReset);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ResetUsersPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetUsersPassword([FromBody]ResetUsersPasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                for (int i = 0; i < model.Usernames.Count(); i++)
                {
                    var isReset = await ResetUserPassword(model.Usernames[i], model.NewPassword);
                    result = ProcessResultViewModelHelper.Succedded(isReset);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        private Task<bool> ResetUserPassword(string userName, string newPassword)
        {
            Task<bool> result = null;
            try
            {
                result = _applicationUserManager.ResetPassword(userName, newPassword);
            }
            catch (Exception ex)
            {
                result = Task.FromResult(false);
            }
            return result;
        }

        [HttpGet, Route("IsPasswordPinExist")]
        public ProcessResultViewModel<bool> IsPasswordPinExist([FromQuery]string pin)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExist = _passwordTokenPinManager.IsPinExist(pin);
                result = ProcessResultViewModelHelper.Succedded(isExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsTokenValid")]
        public ProcessResultViewModel<bool> IsTokenValid()
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                bool isValid = TokenManager.ValidateToken(token);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetByIds")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByIds([FromBody]List<string> ids)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetByIds(ids);
                var models = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("AssignToRole")]
        public async Task<ProcessResultViewModel<bool>> AssignToRole([FromBody]AssignToRoleViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(model.UserId);
                user.RoleNames = model.Roles.ToList();
                var isSucceed = await _applicationUserManager.AddUserToRolesAsync(user);
                result = ProcessResultViewModelHelper.Succedded(isSucceed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("DownloadReportFile")]
        public async Task<IActionResult> DownloadReportFile([FromQuery] string fileName)
        {
            var webRootInfo = _hostingEnv.ContentRootPath;
            var dirPath = webRootInfo + @"/wwwroot/Reports";
            string filePath = $"{dirPath}/{fileName}";
            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, FileUtilities.GetContentType(filePath), Path.GetFileName(filePath));
        }

        //[HttpGet, Route("UpdateUserLanguage")]
        //public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserLanguage([FromQuery]SupportedLanguage languageId)
        //{
        //    string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
        //    string token = authorizationValue.Substring("Bearer ".Length).Trim();
        //    string userId = TokenManager.GetUserIdFromToken(token);
        //    var user = await _applicationUserManager.UpdateUserLanguage(userId, languageId);
        //    if (user != null)
        //    {
        //        var userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
        //        return ProcessResultViewModelHelper.Succedded(userViewModel);
        //    }
        //    else
        //    {
        //        return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "An error occured while updating user");
        //    }
        //}

        [HttpGet, Route("GetDevices/{userId}")]
        public ProcessResultViewModel<List<UserDevice>> GetDevices([FromRoute] string userId)
        {
            var result = new ProcessResultViewModel<List<UserDevice>>();
            try
            {
                var userDevices = _userDeviceManager.GetByUserId(userId);
                result = ProcessResultViewModelHelper.Succedded(userDevices);
            }
            catch (Exception e)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserDevice>>(null, e.Message);
            }
            return result;
        }

    }
}
