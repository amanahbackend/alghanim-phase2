﻿using AutoMapper;
using Ghanim.Identity.API.ViewModels;
using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace Ghanim.Identity.API.Utilities
{
    public static class TokenManager
    {
        /// this key is sha256 encoding of "mawashiSecret" 
        private static string _jwtSecurityKey = "2dfcdfebc9f0037512964af4f0939bc715616e6609574bf43665e19f9cbc1ecd";

        public static async Task<Tuple<ApplicationUserViewModel, JwtSecurityToken>> GetToken(
           LoginViewModel model, IApplicationUserManager applicationUserManager,
           IPasswordHasher<ApplicationUser> passwordHasher, IUserDeviceManager userDeviceManager)
        {
            ApplicationUser user = await applicationUserManager.GetBy(model.Username);
            if (user != null)
            {
                bool deactivated = await applicationUserManager.IsUserDeactivated(model.Username);
                if (!deactivated)
                {
                    bool phoneCofirmed = await applicationUserManager.IsPhoneConfirmed(model.Username);
                    if (phoneCofirmed)
                    {
                        var passwordCheckResult = passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password);
                        if (passwordCheckResult == PasswordVerificationResult.Success)
                        {
                            var userClaims = await applicationUserManager.GetClaimsAsync(user);

                            var claims = new[]
                                {
                                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                new Claim(JwtRegisteredClaimNames.Jti, user.Id),
                                new Claim(JwtRegisteredClaimNames.Email, user.Email)
                            }.Union(userClaims);

                            byte[] jwtSecurityTokenBytes = Convert.FromBase64String(_jwtSecurityKey);
                            var symmetricSecurityKey = new SymmetricSecurityKey(jwtSecurityTokenBytes);
                            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

                            var token = new JwtSecurityToken(
                                    claims: claims,
                                    expires: DateTime.UtcNow.AddYears(15),
                                    signingCredentials: signingCredentials);

                            var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                            if (!string.IsNullOrEmpty(model.DeviceId))
                            {
                                userDeviceManager.AddIfNotExist(new UserDevice { DeveiceId = model.DeviceId, Fk_AppUser_Id = user.Id });
                            }
                            return new Tuple<ApplicationUserViewModel, JwtSecurityToken>(userModel, token);
                        }
                    }
                }
            }
            return null;
        }

        public static bool ValidateToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                var symmetricKey = Convert.FromBase64String(_jwtSecurityKey);

                var validationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out SecurityToken securityToken);
                return principal.Identity.IsAuthenticated;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<List<string>> GetRolesFromToken(string token, IApplicationUserManager applicationUserManager)
        {
            JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
            string username = jwtTokenObj.Payload.Sub;

            var user = await applicationUserManager.GetBy(username);
            return user?.RoleNames;
        }

        public static string GetUserIdFromToken(string token)
        {
            JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
            string userId = jwtTokenObj.Payload.Jti;
            return userId;
        }
    }
}
