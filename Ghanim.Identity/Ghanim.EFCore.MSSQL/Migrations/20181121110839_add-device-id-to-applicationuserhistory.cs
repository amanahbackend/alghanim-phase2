﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Identity.EFCore.MSSQL.Migrations
{
    public partial class adddeviceidtoapplicationuserhistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DeveiceId",
                table: "ApplicationUserHistory",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeveiceId",
                table: "ApplicationUserHistory");
        }
    }
}
