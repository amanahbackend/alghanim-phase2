﻿using Ghanim.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Identity.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        void AddIfNotExist(UserDevice userDevice);
        void Delete(string deviceId);
        List<UserDevice> GetByUserId(string userId);
    }
}
