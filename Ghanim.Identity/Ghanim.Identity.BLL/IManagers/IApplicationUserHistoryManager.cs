﻿using DispatchProduct.RepositoryModule;
using Ghanim.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Identity.BLL.IManagers
{
   public interface IApplicationUserHistoryManager : IRepository<ApplicationUserHistory>
    {
        ProcessResult<List<ApplicationUserHistory>> GetLoginUser(string userId, string deviceId);
        Task<ProcessResult<List<ApplicationUserHistory>>> GetLoginUsers();
        ProcessResult<List<ApplicationUserHistory>> IsTokenExpired(string token);
    }
}
