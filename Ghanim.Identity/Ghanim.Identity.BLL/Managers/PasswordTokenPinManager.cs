﻿using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Context;
using Ghanim.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ghanim.Identity.BLL.Managers
{
    public class PasswordTokenPinManager : IPasswordTokenPinManager
    {
        private ApplicationDbContext _dbContext;
        public PasswordTokenPinManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public PasswordTokenPin Add(PasswordTokenPin entity)
        {
            if (entity.Pin != null && entity.Token != null)
            {
                entity = _dbContext.PasswordTokenPins.Add(entity).Entity;
                _dbContext.SaveChanges();
                return entity;
            }
            else
            {
                return null;
            }
        }

        public PasswordTokenPin GetByToken(string token)
        {
            return _dbContext.PasswordTokenPins.FirstOrDefault(x => x.Token.Equals(token));
        }

        public PasswordTokenPin GetByPin(string pin)
        {
            return _dbContext.PasswordTokenPins.FirstOrDefault(x => x.Pin.Equals(pin));
        }

        public bool Delete(PasswordTokenPin entity)
        {
            _dbContext.PasswordTokenPins.Remove(entity);
            return _dbContext.SaveChanges() > 0;
        }

        public bool IsPinExist(string pin)
        {
            return _dbContext.PasswordTokenPins.Where(x => x.Pin.Equals(pin)).Count() > 0;
        }
    }
}
