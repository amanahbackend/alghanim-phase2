﻿using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Context;
using Ghanim.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ghanim.Identity.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Delete(string deviceId)
        {
            var userDeviceRes = _dbContext.UserDevices.FirstOrDefault(x => x.DeveiceId == deviceId);
            if (userDeviceRes!=null)
            {
               _dbContext.UserDevices.Remove(userDeviceRes);
                _dbContext.SaveChanges();
            }
        }

        public void AddIfNotExist(UserDevice userDevice)
        {
            var count = _dbContext.UserDevices.Count(x => x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id);
            if (count > 0)
            {
                var isDeviceExist = _dbContext.UserDevices.Count(x =>
                                        x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id &&
                                        x.DeveiceId == userDevice.DeveiceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevices.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevices.Add(userDevice);
            }
            _dbContext.SaveChanges();
        }

        public List<UserDevice> GetByUserId(string userId)
        {
            var userDevices = _dbContext.UserDevices.Where(x => x.Fk_AppUser_Id == userId).ToList();
            return userDevices;
        }
    }
}
