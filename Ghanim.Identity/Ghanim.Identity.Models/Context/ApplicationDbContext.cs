﻿using Ghanim.Identity.Models.Entities;
using Ghanim.Identity.Models.EntitiesConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Identity.Models.Context
{
  public  class ApplicationDbContext:IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<PasswordTokenPin> PasswordTokenPins { get; set; }
        public DbSet<ApplicationUserHistory> ApplicationUserHistory { get; set; }
        public DbSet<JunkUser> JunkUsers { get; set; }
        public DbSet<Privilege> Privileges { get; set; }
        public DbSet<RolePrivilege> RolePrivileges { get; set; }
        public DbSet<UserDevice> UserDevices { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new JunkUserTypeConfiguration());
            builder.ApplyConfiguration(new PasswordTokenPinTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationUserTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationUserHistoryTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationRoleTypeConfiguration());
            builder.ApplyConfiguration(new PrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new RolePrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new UserDeviceTypeConfiguration());

        }
    }
}
