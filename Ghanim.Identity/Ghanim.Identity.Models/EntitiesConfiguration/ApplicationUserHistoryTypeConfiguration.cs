﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Identity.Models.EntitiesConfiguration
{
    class ApplicationUserHistoryTypeConfiguration : BaseEntityTypeConfiguration<ApplicationUserHistory>, IEntityTypeConfiguration<ApplicationUserHistory>
    {
        public override void Configure(EntityTypeBuilder<ApplicationUserHistory> ApplicationUserHistoryConfiguration)
        {
            base.Configure(ApplicationUserHistoryConfiguration);

            ApplicationUserHistoryConfiguration.ToTable("ApplicationUserHistory");
            ApplicationUserHistoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            ApplicationUserHistoryConfiguration.Property(o => o.LoginDate).IsRequired();
            ApplicationUserHistoryConfiguration.Property(o => o.Token).IsRequired();
            ApplicationUserHistoryConfiguration.Property(o => o.UserId).IsRequired();
            ApplicationUserHistoryConfiguration.Property(o => o.UserType).IsRequired();
            ApplicationUserHistoryConfiguration.Property(o => o.LogoutDate).IsRequired(false);
            ApplicationUserHistoryConfiguration.Property(o => o.DeveiceId).IsRequired(false);

        }
    }
}
