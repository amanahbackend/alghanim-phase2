﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Order.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.Entities
{
    public class OrderAction : BaseEntity, IOrderAction
    {
        public int? OrderId { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public int? SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public DateTime ActionDate { get; set; }
        public DateTime ActionDay { get; set; }
        public TimeSpan? ActionTime { get; set; }
        public int? CreatedUserId { get; set; }
        public string CreatedUser { get; set; }
        public int? ActionTypeId { get; set; }
        public string ActionTypeName { get; set; }
        public string CostCenterName { get; set; }
        public int? CostCenterId { get; set; }
        public int? WorkingTypeId{ get; set; }
        public string WorkingTypeName{ get; set; }
        public string Reason { get; set; }
        // assigning 
        public double ActionDistance { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int ForemanId { get; set; }
        public string ForemanName { get; set; }
        public int TeamId { get; set; }
        //public int DriverId { get; set; }
        //public string DriverName { get; set; }
        //public string DriverPFNumber { get; set; }
    }
}
