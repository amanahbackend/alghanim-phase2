﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.Models.IEntities
{
   public interface INotificationCenter : IBaseEntity
    {
         string RecieverId { get; set; }
         string Title { get; set; }
         string Body { get; set; }
         int Data { get; set; }
         string NotificationType { get; set; }
         bool IsRead { get; set; }

    }
}
