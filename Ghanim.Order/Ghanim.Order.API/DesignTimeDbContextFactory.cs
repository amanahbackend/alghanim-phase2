﻿using Ghanim.Order.EFCore.MSSQL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<OrderDBContext>
    {
        public OrderDBContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<OrderDBContext>();

            // var connectionString = configuration.GetConnectionString("ConnectionString");

            //builder.UseSqlServer("Data Source=192.168.1.18,1433;Initial Catalog=Ghanim.DataManagement;User ID=sa;Password=123456");
            builder.UseSqlServer("Data Source=tcp:10.0.75.1,1433;Initial Catalog=Ghanim.Order;User ID=sa;Password=123456");

            return new OrderDBContext(builder.Options);
        }
    }
}
