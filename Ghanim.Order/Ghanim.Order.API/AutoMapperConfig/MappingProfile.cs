﻿using AutoMapper;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace Ghanim.Order.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<OrderObject, OrderViewModel>();
            CreateMap<OrderViewModel, OrderObject>()
                .IgnoreBaseEntityProperties();

            CreateMap<OrderObject, ArchivedOrder>()
               .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<ArchivedOrder, OrderObject>()
                .IgnoreBaseEntityProperties();

            CreateMap<WorkingViewModel, TechnicianStateViewModel>();

            CreateMap<TeamViewModel, TeamOrdersViewModel>()
                .ForMember(dest=>dest.TeamId , opt=>opt.MapFrom(src=>src.Id))
                .ForMember(dest=>dest.TeamName , opt=>opt.MapFrom(src=>src.Name))
                .ForMember(dest=>dest.Orders , opt=> opt.Ignore());

        }
    }
}
