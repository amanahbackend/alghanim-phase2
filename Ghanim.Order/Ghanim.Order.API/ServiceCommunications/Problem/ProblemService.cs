﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.Problem
{
    public class ProblemService : DefaultHttpClientCrud<DataManagementServiceSettings, OrderProblemViewModel, OrderProblemViewModel>,
       IProblemService
    {
        DataManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public ProblemService(IOptions<DataManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }
        public async Task<ProcessResultViewModel<List<OrderProblemViewModel>>> GetProblems()
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetProblems;
                var url = $"{baseUrl}/{requestedAction}";
               return await GetByUriCustomized<List<OrderProblemViewModel>>(url);
            }
            return null;
        }
    }
}
