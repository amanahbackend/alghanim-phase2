﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
    public class DropoutServiceSettings : DefaultHttpClientSettings
    {
        public string ServiceName { get; set; }
        public string SendNotifications { get; set; }
        public string SendNotification { get; set; }
        public string SendSMS { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
