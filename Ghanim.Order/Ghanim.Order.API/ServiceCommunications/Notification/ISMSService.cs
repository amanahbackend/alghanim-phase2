﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
    public interface ISMSService : IDefaultHttpClientCrud<DropoutServiceSettings, SMSViewModel, bool>
    {
        Task<ProcessResultViewModel<bool>> Send(SMSViewModel Model);
    }
}
