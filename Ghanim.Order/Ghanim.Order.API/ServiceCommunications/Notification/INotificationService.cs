﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.Notification
{
     public interface INotificationService : IDefaultHttpClientCrud<DropoutServiceSettings, SendNotificationViewModel, string>
    {
        Task<ProcessResultViewModel<string>> SendNotifications(List<SendNotificationViewModel> lstModel);
        Task<ProcessResultViewModel<string>> SendNotification(SendNotificationViewModel Model);
    }
}
