﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System.Threading.Tasks;


namespace Ghanim.Order.API.ServiceCommunications.UserManagementService
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        public string ServiceName { get; set; }
        public string GetUserById { get; set; }
        
        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}