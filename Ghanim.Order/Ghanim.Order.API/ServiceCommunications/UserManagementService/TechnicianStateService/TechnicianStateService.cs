﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public class TechnicianStateService : DefaultHttpClientCrud<UserManagementServiceSettings, TechnicianStateViewModel, TechnicianStateViewModel>,
       ITechnicianStateService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public TechnicianStateService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<TechnicianStateViewModel>> Add(TechnicianStateViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddTechnicianState;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<TechnicianStateViewModel,TechnicianStateViewModel>(url, model);
            }
            return null;
        }
    }
}
