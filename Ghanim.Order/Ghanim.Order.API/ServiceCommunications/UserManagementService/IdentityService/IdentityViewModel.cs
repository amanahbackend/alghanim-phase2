﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService
{
    public class IdentityViewModel : BaseEntity
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string SecondPhone { get; set; }
        public string PicturePath { get; set; }  
        public string Password { get; set; }
      
        public List<string> RoleNames { get; set; }
        public string Picture { get; set; }
        public string PictureUrl { get; set; }
        public bool IsBasicRegister { get; set; }
    }
}
