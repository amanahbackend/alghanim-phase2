﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService
{
    public interface IForemanService : IDefaultHttpClientCrud<UserManagementServiceSettings, ForemanViewModel, ForemanViewModel>
    {                                                               
        Task<ProcessResultViewModel<ForemanViewModel>> GetForemanByUserId(string userId);
    }
}