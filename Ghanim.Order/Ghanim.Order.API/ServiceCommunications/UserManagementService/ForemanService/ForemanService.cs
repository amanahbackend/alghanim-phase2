﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService
{
    public class ForemanService : DefaultHttpClientCrud<UserManagementServiceSettings, ForemanViewModel, ForemanViewModel>,
       IForemanService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public ForemanService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<ForemanViewModel>> GetForemanByUserId(string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetForemanByUserId;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                return await GetByUriCustomized<ForemanViewModel>(url);
            }
            return null;
        }
    }
}
