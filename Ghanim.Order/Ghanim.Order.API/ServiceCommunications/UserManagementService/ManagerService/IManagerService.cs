﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService
{
    public interface IManagerService : IDefaultHttpClientCrud<UserManagementServiceSettings, ManagerViewModel, ManagerViewModel>
    {
        Task<ProcessResultViewModel<ManagerViewModel>> GetManagerByUserId(string userId);
    }
}