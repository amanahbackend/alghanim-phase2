﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService
{
    public interface IEngineerService : IDefaultHttpClientCrud<UserManagementServiceSettings, EngineerViewModel, EngineerViewModel>
    {
        Task<ProcessResultViewModel<EngineerViewModel>> GetEngineerByUserId(string userId);
    }
}