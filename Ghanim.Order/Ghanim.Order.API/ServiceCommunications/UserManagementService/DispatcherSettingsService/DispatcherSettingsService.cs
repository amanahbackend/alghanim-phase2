﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public class DispatcherSettingsService : DefaultHttpClientCrud<UserManagementServiceSettings, DispatcherSettingsViewModel, DispatcherSettingsViewModel>,
       IDispatcherSettingsService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public DispatcherSettingsService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<List<DispatcherSettingsViewModel>>> GetDispatcherSettingsByOrder(SettingsViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetDispatcherSettings;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<SettingsViewModel, List<DispatcherSettingsViewModel>>(url, model);
            }
            return null;
        }
    }
}
