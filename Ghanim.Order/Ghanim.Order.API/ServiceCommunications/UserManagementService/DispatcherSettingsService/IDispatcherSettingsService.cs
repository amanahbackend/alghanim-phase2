﻿using DispatchProduct.HttpClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public interface IDispatcherSettingsService : IDefaultHttpClientCrud<UserManagementServiceSettings, DispatcherSettingsViewModel, DispatcherSettingsViewModel>
    {
        Task<ProcessResultViewModel<List<DispatcherSettingsViewModel>>> GetDispatcherSettingsByOrder(SettingsViewModel model);
    }
}
