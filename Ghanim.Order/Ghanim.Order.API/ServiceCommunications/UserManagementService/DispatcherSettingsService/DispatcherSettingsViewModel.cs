﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public class DispatcherSettingsViewModel : BaseEntity
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int OrderProblemId { get; set; }
        public string OrderProblemName { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
