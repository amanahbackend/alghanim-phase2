﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService
{
    public class TeamService : DefaultHttpClientCrud<UserManagementServiceSettings, TeamViewModel, TeamViewModel>,
       ITeamService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public TeamService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherId(int dispatcherId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetTeamsByDispatcherId;
                var url = $"{baseUrl}/{requestedAction}/{dispatcherId}";
                return await GetByUriCustomized<List<TeamViewModel>>(url);
            }
            return null;
        }
    }
}
