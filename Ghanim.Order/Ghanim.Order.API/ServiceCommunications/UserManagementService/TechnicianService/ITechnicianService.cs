﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService
{
    public interface ITechnicianService : IDefaultHttpClientCrud<UserManagementServiceSettings, TechnicianViewModel, TechnicianViewModel>
    {
        Task<ProcessResultViewModel<TechnicianViewModel>> GetTechnicianByUserId(string userId);
    }
}