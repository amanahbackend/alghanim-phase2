﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using CommonEnums;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.ServiceCommunications.Notification;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Ghanim.Order.API.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Utilites.ProcessingResult;
using Utilities;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : BaseController<IOrderManager, OrderObject, OrderViewModel>
    {
        public IOrderManager manager;
        public IOrderSettingManager settingManager;
        public IOrderActionManager actionManager;
        public readonly new IMapper mapper;
        private readonly IConfigurationRoot _configuration;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IDispatcherService dispatcherService;
        ISupervisorService supervisorService;
        ITeamMembersService teamMembersService;
        INotificationService notificationService;
        ISMSService smsService;
        ITeamService teamService;
        IIdentityService identityService;
        IForemanService formanService;
        IEngineerService engineerService;
        IManagerService managerService;
        ITechnicianService technicianService;
        IAPIHelper helper;
        ITechnicianStateService technicianStateService;
        public OrderController(IOrderManager _manager,
            IOrderSettingManager _settingManager,
            IOrderActionManager _actionManager,
            IAPIHelper _helper,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService,
            IDispatcherService _dispatcherService,
            INotificationService _notificationService,
            ITeamService _teamService,
            IIdentityService _identityService,
            ISupervisorService _supervisorService,
            IForemanService _formanService,
            IEngineerService _engineerService,
            IManagerService _managerService,
            ITechnicianService _technicianService,
            ITechnicianStateService _technicianStateService,
            ISMSService _smsService
            ) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            settingManager = _settingManager;
            actionManager = _actionManager;
            mapper = _mapper;
            helper = _helper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            dispatcherService = _dispatcherService;
            teamMembersService = _teamMembersService;
            notificationService = _notificationService;
            teamService = _teamService;
            supervisorService = _supervisorService;
            identityService = _identityService;
            formanService = _formanService;
            engineerService = _engineerService;
            managerService = _managerService;
            technicianService = _technicianService;
            technicianStateService = _technicianStateService;
            smsService = _smsService;
        }

        [HttpPost]
        [Route("SetAcceptence")]
        public async Task<ProcessResultViewModel<bool>> SetAcceptence([FromBody]AcceptenceViewModel model)
        {
            if (model.OrderId > 0)
            {
                //var entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason);
                //if (entityResult.IsSucceeded)
                //{
                //    var order = Get(model.OrderId).Data;
                //    var orderObject = Mapper.Map<OrderObject>(order);
                //    if (model.AcceptenceFlag == true)
                //    {
                //        await AddAction(orderObject, OrderActionType.Acceptence, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                //    }
                //    else
                //    {
                //        await AddAction(orderObject, OrderActionType.Rejection, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                //    }

                //    //notify dispatcher with acceptance or rejection

                //    var NotificationRes = await NotifyDispatcherWithOrderAcceptence(model);
                //    return ProcessResultViewModelHelper.Succedded<bool>(true);
                //}
                //else
                //{
                //    return ProcessResultViewModelHelper.Failed<bool>(false, entityResult.Status.Message);
                //}

                var acceptenceResult = await ChangeAcceptence(model);
                if (acceptenceResult.IsSucceeded && acceptenceResult.Data == true)
                {
                    var NotificationRes = await NotifyDispatcherWithOrderAcceptence(model);
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, acceptenceResult.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "order id is required");
            }
        }

        private async Task<ProcessResult<bool>> ChangeAcceptence(AcceptenceViewModel model)
        {
            ProcessResult<bool> entityResult = new ProcessResult<bool>();
            if (model.AcceptenceFlag == true)
            {
                entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason, null, null, null, null);
            }
            else
            {
                entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name, StaticAppSettings.InitialSubStatus.Id, StaticAppSettings.InitialSubStatus.Name);
            }
            if (entityResult.IsSucceeded)
            {
                var order = Get(model.OrderId).Data;
                var orderObject = Mapper.Map<OrderObject>(order);
                if (model.AcceptenceFlag == true)
                {
                    await AddAction(orderObject, OrderActionType.Acceptence, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                }
                else
                {
                    await AddAction(orderObject, OrderActionType.Rejection, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                }
            }
            return entityResult;
        }

        [HttpPut]
        [Route("ChangeStatus")]
        public async Task<ProcessResult<bool>> ChangeStatus([FromBody] ChangeStatusViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id is required");
            }

            var orderResult = manager.Get(model.OrderId);

            if (orderResult.IsSucceeded && orderResult.Data != null)
            {
                orderResult.Data.StatusId = model.StatusId;
                orderResult.Data.StatusName = model.StatusName;
                orderResult.Data.SubStatusId = model.SubStatusId;
                orderResult.Data.SubStatusName = model.SubStatusName;
                if (StaticAppSettings.TravellingStatusId.Contains(model.StatusId))
                {
                    // send sms here
                    string msgContent = string.Format(StaticAppSettings.SMSContent, orderResult.Data.Code, model.StatusName);
                    var sms = new SMSViewModel()
                    {
                        PhoneNumber = orderResult.Data.PhoneOne,
                        Message = msgContent
                    };
                    var smsResult = await smsService.Send(sms);
                }

                // make order unassigned incase it changed to on hold
                if (StaticAppSettings.UnassignedStatusId.Contains(model.StatusId))
                {
                    orderResult.Data.TeamId = 0;
                    orderResult.Data.RankInTeam = 0;
                }

                var updateResult = manager.Update(orderResult.Data);
                if (updateResult.IsSucceeded)
                {
                    await AddAction(orderResult.Data, OrderActionType.ChangeStatus, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual));
                    //notify teamMembers and dispatcher with new status
                    var NotificationRes = await NotifyTeamMembersandDispatcherWithNewStatus(model);
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, "something worng while update order");
                }
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id is required");
            }
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<OrderViewModel>> PostMulti([FromBody] List<OrderViewModel> orders)
        {
            for (int i = 0; i < orders.Count; i++)
            {
                // Check for repeated call
                DateTime currentTime = DateTime.Now;

                var customerOrders = manager.GetAll(x => x.CustomerCode == orders[i].CustomerCode && (currentTime - x.CreatedDate).TotalHours <= 24);
                if (customerOrders.IsSucceeded && customerOrders.Data.Count > 0)
                {
                    orders[i].IsRepeatedCall = true;
                }

            }
            var addedOrders = base.PostMulti(orders);
            if (addedOrders.IsSucceeded && addedOrders.Data.Count > 0)
            {
                var mappedOrders = mapper.Map<List<OrderViewModel>, List<OrderObject>>(addedOrders.Data);
                for (int i = 0; i < mappedOrders.Count; i++)
                {
                    AddAction(mappedOrders[i], OrderActionType.AddOrder, null, null);
                }
            }

            return addedOrders;
        }

        [HttpPut]
        [Route("BulkAssign")]
        public async Task<ProcessResult<bool>> BulkAssign([FromBody] BulkAssignViewModel model)
        {
            if (model.AssignType == AssignType.Dispatcher && model.DispatcherId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "dispatcher id is required while assign type is to dispatcher");
            }
            else if (model.AssignType == AssignType.Team && model.TeamId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id is required while assign type is to team");
            }

            if (model.AssignType == AssignType.Dispatcher)
            {
                var dispatcher = await dispatcherService.GetDispatcherById((int)model.DispatcherId);
                if (dispatcher.IsSucceeded && dispatcher.Data != null)
                {
                    return manager.BulkAssign(model.OrderIds, model.AssignType, model.TeamId, model.DispatcherId, dispatcher.Data.Name, dispatcher.Data.SupervisorId, dispatcher.Data.SupervisorName, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name);

                }
                return ProcessResultHelper.Failed<bool>(false, null, "someting wrong while getting dispatcher information", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkAssign Controler");
            }
            else
            {
                return manager.BulkAssign(model.OrderIds, model.AssignType, model.TeamId, model.DispatcherId, null, null, null, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name);
            }
        }

        [HttpPut]
        [Route("Assign")]
        public async Task<ProcessResult<bool>> Assign([FromBody] AssignViewModel model)
        {
            if (model.OrderId == 0 && model.TeamId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id & team id is required");
            }

            var order = Get(model.OrderId).Data;
            var assignResult = new ProcessResult<bool>();
            if (order.StatusId == StaticAppSettings.OnHoldStatus.Id)
            {
                assignResult = manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[1], StaticAppSettings.DispatchedSubStatusNames[1]);
            }
            else
            {
                assignResult = manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[0], StaticAppSettings.DispatchedSubStatusNames[0]);
            }

            if (assignResult.IsSucceeded & assignResult.Data == true)
            {
                var orderObject = Mapper.Map<OrderObject>(order);

                AddAction(orderObject, OrderActionType.Assign, null, null);

                var notifyTeamRes = await NotifyTeamMembersWithNewOrder(model.TeamId, order);
                return ProcessResultHelper.Succedded<bool>(true);
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "something wrong while assign order to team");
            }
        }


        [HttpPut]
        [Route("UnAssign")]
        public async Task<ProcessResult<bool>> UnAssign([FromBody] UnAssignViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id is required");
            }
            var orderRes = manager.Get(model.OrderId);
            int TeamId = orderRes.Data.TeamId;
            if (StaticAppSettings.CanUnassignStatusName.Contains(orderRes.Data.StatusName))
            {
                var unAssignResult = new ProcessResult<bool>();

                if (orderRes.Data.StatusId == StaticAppSettings.OnHoldStatus.Id)
                {
                    unAssignResult = manager.UnAssign(model.OrderId, StaticAppSettings.OnHoldStatus.Id, StaticAppSettings.OnHoldStatus.Name);
                }
                else
                {
                    unAssignResult = manager.UnAssign(model.OrderId, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name);
                }

                if (unAssignResult.IsSucceeded & unAssignResult.Data == true)
                {
                    var order = Get(model.OrderId).Data;
                    var orderObject = Mapper.Map<OrderObject>(order);

                    AddAction(orderObject, OrderActionType.UnAssgin, null, null);

                    var notifyTeamRes = await NotifyTeamMembersWithUnassignOrder(TeamId, model.OrderId);
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, "something wrong while unassign order");
                }
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "couldn't unassign this order with this status");
            }

        }

        [HttpPut]
        [Route("BulkUnAssign")]
        public ProcessResult<bool> BulkUnAssign([FromBody] List<int> model)
        {
            if (model == null || model.Count == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order ids is required while unassign orders");
            }

            return manager.BulkUnAssign(model, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name);
        }

        [HttpPut]
        [Route("ChangeTeamRank")]
        public ProcessResult<bool> ChangeTeamRank([FromBody] ChangeRankInTeamViewModel model)
        {
            if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
            }

            var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
            {
                return manager.ChangeTeamRank(teamOrders.Data, model.Orders);
            }

            return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }

        [HttpGet]
        [Route("GetTeamOrders")]
        public ProcessResult<List<OrderViewModel>> GetTeamOrders([FromQuery] int teamId)
        {
            if (teamId > 0)
            {
                var orders = manager.GetAllQuerable().Data.Where(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId)).OrderBy(x => x.RankInTeam).ToList();
                var result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders);

                return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "team id is required", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }

        [HttpGet]
        [Route("GetAssignTypes")]
        public ProcessResult<List<EnumEntity>> GetAssignTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AssignType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAcceptenceTypes")]
        public ProcessResult<List<EnumEntity>> GetAcceptenceTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AcceptenceType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAccomplishTypes")]
        public ProcessResult<List<EnumEntity>> GetAccomplishTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AccomplishType>.GetEnumList());
        }

        [HttpPost]
        [Route("Search")]
        public ProcessResult<List<OrderViewModel>> Search([FromBody] FilterOrderViewModel model)
        {

            if (model != null && model.Filters.Count > 0)
            {
                Expression<Func<OrderObject, bool>> predicateFilter = null;
                if (model != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters, model.IncludeUnAssign);
                }

                var result = manager.GetAll(predicateFilter);
                if (result.IsSucceeded && result.Data.Count > 0)
                {
                    List<OrderViewModel> searchResult = mapper.Map<List<OrderObject>, List<OrderViewModel>>(result.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(searchResult);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(new List<OrderViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "filter properties are required at least one property");
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForDispatcher")]
        public ProcessResult<List<OrderViewModel>> GetUnAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        {

            if (dispatcherId > 0)
            {
                var orders = manager.GetAll(x => x.DispatcherId == dispatcherId && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(new List<OrderViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForSupervisor")]
        public ProcessResult<List<OrderViewModel>> GetUnAssignedOrdersForSupervisor([FromQuery] int supervisrId)
        {

            if (supervisrId > 0)
            {
                var orders = manager.GetAll(x => x.SupervisorId == supervisrId && x.DispatcherId == 0 && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(new List<OrderViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "supervisor id is required");
        }

        [HttpGet]
        [Route("GetAssignedOrdersForDispatcher")]
        public async Task<ProcessResult<List<TeamOrdersViewModel>>> GetAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        {
            if (dispatcherId > 0)
            {
                List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
                deactiveStatusesId.Add(StaticAppSettings.InitialStatus.Id);

                var orders = manager.GetAll(x => x.DispatcherId == dispatcherId && x.TeamId > 0 && !deactiveStatusesId.Contains(x.StatusId));
                var dispatcherTeams = await teamService.GetTeamsByDispatcherId(dispatcherId);
                var currentTeams = mapper.Map<List<TeamViewModel>, List<TeamOrdersViewModel>>(dispatcherTeams.Data);

                if (orders.IsSucceeded)
                {
                    if (orders.Data.Count > 0)
                    {
                        List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                        var groups = result.GroupBy(x => x.TeamId, (key, group) => new TeamOrdersViewModel { TeamId = key, Orders = group.OrderBy(x => x.RankInTeam) }).ToList();
                        if (groups.Count > 0)
                        {
                            if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                            {

                                for (int i = 0; i < currentTeams.Count; i++)
                                {
                                    currentTeams[i].Orders = new List<OrderViewModel>();
                                    for (int j = 0; j < groups.Count; j++)
                                    {
                                        if (currentTeams[i].TeamId == groups[j].TeamId)
                                        {
                                            currentTeams[i].Orders = groups[j].Orders;
                                        }
                                    }
                                }
                            }
                        }
                        return ProcessResultHelper.Succedded<List<TeamOrdersViewModel>>(currentTeams);
                    }
                }
                return ProcessResultHelper.Succedded<List<TeamOrdersViewModel>>(currentTeams);
            }
            return ProcessResultHelper.Failed<List<TeamOrdersViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpGet]
        [Route("GetAssignedOrdersForDispatchersBySupervisor")]
        public ProcessResult<List<DispatchersUnAssignedOrdersViewModel>> GetAssignedOrdersForDispatchersBySupervisor([FromQuery] int supervisorId)
        {
            if (supervisorId > 0)
            {
                var orders = manager.GetAll(x => x.SupervisorId == supervisorId && x.DispatcherId > 0 && x.StatusId == StaticAppSettings.InitialStatus.Id);

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    var groups = result.GroupBy(x => x.DispatcherId, (key, group) => new DispatchersUnAssignedOrdersViewModel { DispatcherId = key, Orders = group }).ToList();
                    return ProcessResultHelper.Succedded<List<DispatchersUnAssignedOrdersViewModel>>(groups);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<DispatchersUnAssignedOrdersViewModel>>(null);
                }
            }
            return ProcessResultHelper.Failed<List<DispatchersUnAssignedOrdersViewModel>>(null, null, "supervisor id is required");
        }


        [HttpGet]
        [Route("GetMapOrdersForDispatcher")]
        public ProcessResult<List<OrderViewModel>> GetMapOrdersForDispatcher([FromQuery] int dispatcherId)
        {
            if (dispatcherId > 0)
            {
                List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;

                var orders = manager.GetAll(x => x.DispatcherId == dispatcherId && x.Long > 0 && x.Lat > 0 && !deactiveStatusesId.Contains(x.StatusId));
                if (orders.IsSucceeded)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<OrderViewModel>>(null, orders.Exception, "Someting worng while getting orders");
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "dispatcher id is required");
        }


        [HttpPost]
        [Route("SetWorkingAction")]
        public async Task<ProcessResultViewModel<bool>> SetWorkingAction([FromBody]WorkingViewModel model)
        {
            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
            }


            var addedOrderAction = await AddAction(null, actionType, model.WorkingTypeId, model.WorkingTypeName);
            if (addedOrderAction.IsSucceeded)
            {
                var technicianState = mapper.Map<WorkingViewModel, TechnicianStateViewModel>(model);
                var result = await technicianStateService.Add(technicianState);
                if (result.IsSucceeded == true && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, result.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, addedOrderAction.Status.Message);
            }
        }

        [HttpPost]
        [Route("SetWorkingState")]
        public async Task<ProcessResultViewModel<bool>> SetWorkingState([FromBody]WorkingViewModel model)
        {

            if (model.State == TechnicianState.Working && model.OrderId == 0)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Order id is required to change team state to working");
            }
            else if (model.WorkingTypeName == "Reject first order" && string.IsNullOrEmpty(model.RejectionReason))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Rejection reason is required to reject the order");
            }

            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
            }

            OrderObject orderObject = new OrderObject();
            if (model.OrderId > 0)
            {
                var order = Get(model.OrderId).Data;
                orderObject = Mapper.Map<OrderObject>(order);
            }


            if (model.State == TechnicianState.Working)
            {
                string acceptValue = StaticAppSettings.WorkingTypes[0];
                string rejectValue = StaticAppSettings.WorkingTypes[1];
                string continueValue = StaticAppSettings.WorkingTypes[2];

                if (acceptValue == model.WorkingTypeName)
                {
                    // accept order and set on travel
                    var acceptAndSetTravelling = manager.AcceptOrderAndSetOnTravel(model.OrderId, StaticAppSettings.TravellingStatusName[0], StaticAppSettings.TravellingStatusId[0], StaticAppSettings.OnTravelSubStatus.Name, StaticAppSettings.OnTravelSubStatus.Id);
                    if (!(acceptAndSetTravelling.IsSucceeded && acceptAndSetTravelling.Data == true))
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, acceptAndSetTravelling.Status.Message);
                    }
                }
                else if (rejectValue == model.WorkingTypeName)
                {
                    // make order as rejected
                    AcceptenceViewModel acceptenceViewModel = new AcceptenceViewModel()
                    {
                        AcceptenceFlag = false,
                        OrderId = model.OrderId,
                        RejectionReason = model.RejectionReason,
                        RejectionReasonId = model.RejectionReasonId
                    };
                    var acceptenceResult = await ChangeAcceptence(acceptenceViewModel);
                    if (!(acceptenceResult.IsSucceeded && acceptenceResult.Data == true))
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, acceptenceResult.Status.Message);
                    }
                }
                else if (continueValue == model.WorkingTypeName)
                {

                }
            }

            ProcessResult<OrderAction> addedOrderAction = new ProcessResult<OrderAction>();
            if (model.OrderId > 0)
            {
                addedOrderAction = await AddAction(orderObject, actionType, model.WorkingTypeId, model.WorkingTypeName);
            }
            else
            {
                addedOrderAction = await AddAction(null, actionType, model.WorkingTypeId, model.WorkingTypeName);
            }

            if (addedOrderAction.IsSucceeded)
            {
                var technicianState = mapper.Map<WorkingViewModel, TechnicianStateViewModel>(model);
                var result = await technicianStateService.Add(technicianState);
                if (result.IsSucceeded == true && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, result.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, addedOrderAction.Status.Message);
            }
        }

        [HttpGet]
        [Route("GetOrderActionTypes")]
        public ProcessResult<List<EnumEntity>> GetOrderActionTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<OrderActionType>.GetEnumList());
        }

        private Expression<Func<OrderObject, bool>> CreateOrdersFilterPredicate(List<Filter> filters, bool includeUnAssign)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderObject>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<OrderObject>(filter.PropertyName, filter.Value));
                }
            }
            if (includeUnAssign)
            {
                predicate = predicate.And(x => !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            }
            else
            {
                List<int> excludeStatusIds = StaticAppSettings.ArchivedStatusId;
                excludeStatusIds.Add(StaticAppSettings.InitialStatus.Id);
                predicate = predicate.And(x => !excludeStatusIds.Contains(x.StatusId));
            }
            return predicate;
        }

        private async Task<ProcessResult<OrderAction>> AddAction(OrderObject order, OrderActionType actionType, int? workingTypeId, string workingTypeName)
        {
            ///// NOTE :: We have to do the below on all of team in case the user is technician
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            var UserRes = await identityService.GetUserById(currentUserId);
            string CostCenterName = null;
            int? CostCenterId = 0;
            int teamId = 0;
            // get user data
            var orderAction = new OrderAction();
            var orderActions = new List<OrderAction>();

            orderAction.ActionDate = DateTime.Now;
            orderAction.ActionDay = DateTime.Now.Date;
            if (UserRes != null && UserRes.Data != null)
            {
                var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
                switch (roleNames)
                {
                    case "Engineer":
                        var engineer = await engineerService.GetEngineerByUserId(currentUserId);
                        if (engineer != null && engineer.Data != null)
                        {
                            CostCenterId = engineer.Data.CostCenterId;
                            CostCenterName = engineer.Data.CostCenterName;
                            orderAction.CreatedUser = engineer.Data.Name;
                        }
                        break;

                    case "Technician":
                        var technician = await technicianService.GetTechnicianByUserId(currentUserId);
                        if (technician != null && technician.Data != null)
                        {
                            CostCenterId = technician.Data.CostCenterId;
                            CostCenterName = technician.Data.CostCenterName;
                            orderAction.TeamId = technician.Data.TeamId;
                            teamId = technician.Data.TeamId;
                            orderAction.CreatedUser = technician.Data.Name;
                            orderAction.CurrentUserId = technician.Data.UserId;


                            var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
                            if (teamMembers.IsSucceeded && teamMembers.Data != null)
                            {
                                for (int i = 0; i < teamMembers.Data.Count; i++)
                                {
                                    if (technician.Data.Name != teamMembers.Data[i].MemberParentName)
                                    {
                                        orderActions.Add(new OrderAction()
                                        {
                                            CostCenterId = teamMembers.Data[i].CostCenterId,
                                            CostCenterName = teamMembers.Data[i].CostCenter,
                                            TeamId = teamMembers.Data[i].TeamId,
                                            ActionDate = orderAction.ActionDate,
                                            CreatedUser = teamMembers.Data[i].MemberParentName,
                                            CurrentUserId = teamMembers.Data[i].UserId,
                                            ActionDay = DateTime.Now.Date
                                        });
                                    }
                                }
                            }

                        }
                        break;

                    case "Foreman":
                        var foreman = await formanService.GetForemanByUserId(currentUserId);
                        if (foreman != null && foreman.Data != null)
                        {
                            CostCenterId = foreman.Data.CostCenterId;
                            CostCenterName = foreman.Data.CostCenterName;
                            orderAction.CreatedUser = foreman.Data.Name;
                        }
                        break;

                    case "Supervisor":
                        var supervisor = await supervisorService.GetSupervisorByUserId(currentUserId);
                        if (supervisor != null && supervisor.Data != null)
                        {
                            CostCenterId = supervisor.Data.CostCenterId;
                            CostCenterName = supervisor.Data.CostCenterName;
                            orderAction.CreatedUser = supervisor.Data.Name;
                        }
                        break;

                    case "Dispatcher":
                        var dispatcher = await dispatcherService.GetDispatcherByUserId(currentUserId);
                        if (dispatcher != null && dispatcher.Data != null)
                        {
                            CostCenterId = dispatcher.Data.CostCenterId;
                            CostCenterName = dispatcher.Data.CostCenterName;
                            orderAction.CreatedUser = dispatcher.Data.Name;
                        }
                        break;

                    default:
                        break;
                }
            }

            // update action time in last
            if (orderAction.TeamId > 0)
            {
                var lastActions = actionManager.GetAll(x => x.TeamId == orderAction.TeamId && x.ActionTime == null).Data.ToList();
                if (lastActions.Count > 0)
                {
                    for (int i = 0; i < lastActions.Count; i++)
                    {
                        lastActions[i].ActionTime = orderAction.ActionDate - lastActions[i].ActionDate;
                    }
                    actionManager.Update(lastActions);
                }
            }

            // binding order action details
            if (order != null)
            {
                orderAction.StatusId = order.StatusId;
                orderAction.StatusName = order.StatusName;
                orderAction.OrderId = order.Id;
                orderAction.SubStatusId = order.SubStatusId;
                orderAction.SubStatusName = order.SubStatusName;
                orderAction.ActionTypeId = (int)actionType;
                orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
                orderAction.SupervisorId = order.SupervisorId;
                orderAction.DispatcherId = order.DispatcherId;
                orderAction.DispatcherName = order.DispatcherName;
                orderAction.SupervisorName = order.SupervisorName;
                orderAction.TeamId = orderAction.TeamId;
                orderAction.CurrentUserId = currentUserId;
                orderAction.CostCenterId = CostCenterId;
                orderAction.CostCenterName = CostCenterName;
            }
            else
            {
                orderAction.ActionTypeId = (int)actionType;
                orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
                orderAction.CurrentUserId = currentUserId;
                orderAction.CostCenterId = CostCenterId;
                orderAction.CostCenterName = CostCenterName;
            }



            if (order != null)
            {
                if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Traveling;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);
                }
                else
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
                }
            }
            else
            {
                orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
                orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
                ///// for time sheet groups
                if (actionType == OrderActionType.Idle)
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Idle;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle);
                    orderAction.Reason = workingTypeName;
                }
                else if (actionType == OrderActionType.Break)
                {
                    orderAction.WorkingTypeId = (int)TimeSheetType.Break;
                    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Break);
                    orderAction.Reason = workingTypeName;
                }
            }

            //if (actionType == OrderActionType.Work)
            //{
            //    if (order != null)
            //    {
            //        if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
            //        {
            //            orderAction.WorkingTypeId = (int)TimeSheetType.Traveling;
            //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);
            //        }
            //        else
            //        {
            //            orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
            //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
            //        }
            //    }
            //    //else
            //    //{
            //    //    if (true)
            //    //    {

            //    //    }
            //    //    return ProcessResultHelper.Failed<OrderAction>(null, null, "order is required");
            //    //}
            //}
            //else
            //{
            //    orderAction.WorkingTypeId = workingTypeId;
            //    orderAction.WorkingTypeName = workingTypeName;
            //}

            if (workingTypeName == "Stop")
            {
                orderAction.Reason = "Stop";
            }

            // binding order actions on list
            if (orderActions.Count > 0)
            {
                for (int i = 0; i < orderActions.Count; i++)
                {
                    if (order != null)
                    {
                        orderActions[i].StatusId = order.StatusId;
                        orderActions[i].StatusName = order.StatusName;
                        orderActions[i].OrderId = order.Id;
                        orderActions[i].SubStatusId = order.SubStatusId;
                        orderActions[i].SubStatusName = order.SubStatusName;
                        orderActions[i].SupervisorId = order.SupervisorId;
                        orderActions[i].DispatcherId = order.DispatcherId;
                        orderActions[i].DispatcherName = order.DispatcherName;
                        orderActions[i].SupervisorName = order.SupervisorName;
                    }
                    orderActions[i].ActionTypeId = orderAction.ActionTypeId;
                    orderActions[i].ActionTypeName = orderAction.ActionTypeName;
                    orderActions[i].TeamId = orderAction.TeamId;
                    orderActions[i].WorkingTypeId = orderAction.WorkingTypeId;
                    orderActions[i].WorkingTypeName = orderAction.WorkingTypeName;
                    orderActions[i].Reason = orderAction.Reason;
                }
                actionManager.Add(orderActions);
            }

            return actionManager.Add(orderAction);
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrders(int teamId, List<int> OrderIds)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await teamMembersService.GetMembersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    foreach (var OrderId in OrderIds)
                    {
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = item.UserId,
                            Title = "New Order",
                            Body = "There is new Order With Id: " + OrderId,
                            Data = new { OrderId = OrderId }
                        });
                    }

                }

                notificationRes = await notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrder(int teamId, OrderViewModel order)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await teamMembersService.GetMembersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = item.UserId,
                        Title = "New Order",
                        Body = $"New order #{order.Code} assigned to your team",
                        Data = new { OrderId = order.Id }
                    });
                }

                notificationRes = await notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithUnassignOrder(int teamId, int OrderId)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await teamMembersService.GetMembersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = item.UserId,
                        Title = "Unassigned Order",
                        Body = $"Order #{OrderId} is unassigned to another team",
                        Data = new { OrderId = OrderId }
                    });
                }

                notificationRes = await notificationService.SendNotifications(notificationModels);
            }
            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrders(int dispatcherId, List<int> OrderIds)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
        //    if (dispatcher != null && dispatcher.Data != null)
        //    {
        //        foreach (var OrderId in OrderIds)
        //        {
        //            notificationModels.Add(new SendNotificationViewModel()
        //            {
        //                UserId = dispatcher.Data.UserId,
        //                Title = "New Order",
        //                Body = "There is new Order With Id: " + OrderId,
        //                Data = new { OrderId = OrderId }
        //            });
        //        }
        //        notificationRes = await notificationService.SendNotifications(notificationModels);
        //    }
        //    return notificationRes;
        //}

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrder(int dispatcherId, int orderId)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    SendNotificationViewModel notificationModel = new SendNotificationViewModel()
        //    {
        //        UserId = dispatcher.Data.UserId,
        //        Title = "New Order",
        //        Body = "There is new Order With Id: " + orderId,
        //        Data = new { OrderId = orderId }
        //    };
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderAcceptence(AcceptenceViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var orderRes = manager.Get(model.OrderId);
            var dispatcher = await dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;
            var orderId = model.OrderId;
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (model.AcceptenceFlag)
            {
                notificationModel = new SendNotificationViewModel()
                {
                    UserId = dispatcher.Data.UserId,
                    Title = "Acceptence Order",
                    Body = "The order No. " + orderRes.Data.Code + " is Accepted",
                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
                };
            }
            else
            {
                notificationModel = new SendNotificationViewModel()
                {
                    UserId = dispatcher.Data.UserId,
                    Title = "Acceptence Order",
                    Body = "The order No. " + orderRes.Data.Code + " is rejected as " + model.RejectionReason,
                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
                };
            }
            notificationRes = await notificationService.SendNotification(notificationModel);
            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderChange(AcceptenceViewModel model)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var orderRes = manager.Get(model.OrderId);
        //    var dispatcher = await dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
        //    SendNotificationViewModel notificationModel;
        //    var userId = dispatcher.Data.UserId;
        //    var orderId = model.OrderId;
        //    if (model.AcceptenceFlag)
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = userId,
        //            Title = "Acceptence Order",
        //            Body = $"The order With Id: { orderId } is accepted",
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    else
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = dispatcher.Data.UserId,
        //            Title = "Acceptence Order",
        //            Body = "The order With Id: " + model.OrderId + "is rejected as " + model.RejectionReason,
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersandDispatcherWithNewStatus(ChangeStatusViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.OrderId);
            var dispatcherRes = await dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
            if (orderRes.Data.TeamId != 0)
            {
                var teamMembers = await teamMembersService.GetMembersByTeamId(orderRes.Data.TeamId);
                if (teamMembers != null && teamMembers.Data != null)
                {
                    foreach (var item in teamMembers.Data)
                    {
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = item.UserId,
                            Title = "New Status",
                            Body = "The status of order No. " + orderRes.Data.Code + " is changed to " + model.StatusName,
                            Data = new { OrderId = model.OrderId }
                        });
                    }
                }
            }
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            notificationModels.Add(new SendNotificationViewModel()
            {
                UserId = dispatcherRes.Data.UserId,
                Title = "New Status",
                Body = "The status of order No. " + orderRes.Data.Code + " is changed to " + model.StatusName,
                //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
            });
            notificationRes = await notificationService.SendNotifications(notificationModels);
            return notificationRes;
        }
    }
}