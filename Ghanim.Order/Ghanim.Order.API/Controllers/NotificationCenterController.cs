﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class NotificationCenterController : BaseController<INotificationCenterManager, NotificationCenter, NotificationCenterViewModel>
    {
        public INotificationCenterManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public NotificationCenterController(INotificationCenterManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

        [HttpPost]
        [Route("GetByRecieverIdAndIsRead")]
        public  ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> GetByRecieverIdAndIsRead([FromBody]PaginatedFilterNotificationViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            var predicate = PredicateBuilder.True<NotificationCenter>();
            
            if (model.RecieverId!=null)
            {
                predicate = predicate.And(r => r.RecieverId==model.RecieverId);
            }
            if (model.IsRead!=null)
            {
                predicate = predicate.And(r => r.IsRead == model.IsRead);
            }

            var paginatedRes = manager.GetAllPaginated(action, predicate);

            return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        }
    }
}