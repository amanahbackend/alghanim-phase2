﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.Order.API.ViewModels;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;
using CommonEnum;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using CommonEnums;

namespace Ghanim.Order.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderActionController : BaseController<IOrderActionManager, OrderAction, OrderActionViewModel>
    {
        public IOrderActionManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;

        ITeamMembersService teamMembersService;
        public OrderActionController(IOrderActionManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            teamMembersService = _teamMembersService;
        }

        [HttpPost]
        [Route("GetOrderProgress")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderActionViewModel>>> GetOrderProgress([FromBody]PaginatedFilterOrderActionViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<OrderActionViewModel>, PaginatedItems<OrderAction>>(model.PageInfo);
            Expression<Func<OrderAction, bool>> predicateFilter = null;
            if (model.Filters != null)
            {
                predicateFilter = CreateFilterPredicate(model.Filters);
            }
            var paginatedActions = manager.GetAllPaginated(action, predicateFilter);
            return processResultMapper.Map<PaginatedItems<OrderAction>, PaginatedItemsViewModel<OrderActionViewModel>>(paginatedActions);
        }

        [HttpGet]
        [Route("GetOrderProgress/{orderId}")]
        public async Task<ProcessResult<List<OrderActionViewModel>>> GetOrderProgressByOrderId([FromRoute]int orderId)
        {
            var result = new List<OrderActionViewModel>();
            var actions = manager.GetAll(x => x.OrderId == orderId);
            if (actions.IsSucceeded)
            {
                result = mapper.Map<List<OrderAction>, List<OrderActionViewModel>>(actions.Data);
                return ProcessResultHelper.Succedded<List<OrderActionViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderActionViewModel>>(result, actions.Exception, "something wrong while getting data");
        }

        [HttpGet]
        [Route("GetTeamMode/{teamId}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int teamId)
        {
            var result = new TeamModeViewModel();
            var actions = manager.GetAll(x => x.TeamId == teamId);
            if (actions.IsSucceeded)
            {
                if (actions.Data != null)
                {
                    var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(actions.Data.LastOrDefault());
                    if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual && lastAction.OrderId == 0)
                    {
                        result.Mode = "Stopped";
                    }
                    else if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual)
                    {
                        result.Mode = "Working";
                    }
                    else
                    {
                        result.Mode = $"{lastAction.WorkingTypeName} - {lastAction.Reason}";
                    }
                    return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
                }
                return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
            }
            return ProcessResultHelper.Failed<TeamModeViewModel>(result, actions.Exception, "something wrong while getting data");
        }

        [HttpGet]
        [Route("ChangeTeamMode/{teamId}/{isHasOrder}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int teamId, [FromRoute] bool isHasOrder)
        {
            var members = await teamMembersService.GetMembersByTeamId(teamId);
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            if (members.IsSucceeded && members.Data != null)
            {
                for (int i = 0; i < members.Data.Count; i++)
                {
                    model = new OrderAction()
                    {
                        ActionDate = DateTime.Now,
                        ActionDay = DateTime.Now.Date,
                        CostCenterId = members.Data[i].CostCenterId,
                        CostCenterName = members.Data[i].CostCenter,
                        TeamId = teamId,
                        CreatedUser = members.Data[i].MemberParentName,
                        CurrentUserId = members.Data[i].UserId,
                        ActionTypeId = (int)OrderActionType.Idle,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Idle),
                        WorkingTypeId = (int)TimeSheetType.Idle,
                        WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle)
                    };

                    if (isHasOrder)
                    {
                        model.Reason = StaticAppSettings.IdleHandling[1].Name;
                    }
                    else
                    {
                        model.Reason = StaticAppSettings.IdleHandling[0].Name;
                    }
                    manager.Add(model);
                }
            }

            TeamModeViewModel result = new TeamModeViewModel()
            {
                Mode = $"{model.WorkingTypeName} - {model.Reason}"
            };

            return ProcessResultHelper.Succedded<TeamModeViewModel>(result);

        }

        [HttpPost]
        [Route("GetTimeSheet")]
        public async Task<ProcessResult<List<TimeSheetStructureViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        {
            if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
            {
                return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
            }
            else
            {
                var predicate = PredicateBuilder.True<OrderAction>();

                //if (!string.IsNullOrEmpty(model.PFNo))
                //{
                //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
                //}

                if (!string.IsNullOrEmpty(model.CostCenter))
                {
                    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
                }

                predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
                predicate = predicate.And(x => x.ActionTime != null);

                var timeSheetRow = manager.GetAll(predicate);

                if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
                {

                    var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(cl => new TimeSheetStructureViewModel()
                    {
                        DayDate = cl.First().ActionDay.Date,
                        Name = cl.First().CreatedUser.ToString(),
                        Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
                        WorkType = cl.First().WorkingTypeName
                    }).ToList();
                    return ProcessResultHelper.Succedded<List<TimeSheetStructureViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "Something wrong while getting data from order action manager");
                }
            }
        }

        private Expression<Func<OrderAction, bool>> CreateFilterPredicate(List<Filter> filters)
        {
            var predicate = PredicateBuilder.True<OrderAction>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
            }
            return predicate;
        }

    }
}