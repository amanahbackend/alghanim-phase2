﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Ghanim.Order.API.AutoMapperConfig;
using Ghanim.Order.API.Helper;
using Ghanim.Order.API.Schedulers;
using Ghanim.Order.API.Schedulers.TaskFlow;
using Ghanim.Order.API.ServiceCommunications;
using Ghanim.Order.API.ServiceCommunications.Notification;
using Ghanim.Order.API.ServiceCommunications.Problem;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.Order.BLL.IManagers;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Ghanim.Order.Models.IEntities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API
{
    public class Startup
    {

        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Service communication section
            services.Configure<DataManagementServiceSettings>(Configuration.GetSection("ServiceCommunications:DataManagement"));
            services.Configure<UserManagementServiceSettings>(Configuration.GetSection("ServiceCommunications:UserManagementService"));
            services.Configure<DropoutServiceSettings>(Configuration.GetSection("ServiceCommunications:DropoutService"));
            services.Configure<IdentityServiceSettings>(Configuration.GetSection("ServiceCommunications:IdentityService"));

            StaticAppSettings.ArchivedStatusId = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusId").Get<List<int>>();
            StaticAppSettings.TravellingStatusId = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusId").Get<List<int>>();
            StaticAppSettings.UnassignedStatusId = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusId").Get<List<int>>();
            StaticAppSettings.CanUnassignStatusId = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusId").Get<List<int>>();
            StaticAppSettings.DispatchedSubStatusIds = Configuration.GetSection("SubStatusNames:DispatchedIds").Get<List<int>>();
            StaticAppSettings.ArchivedStatusName = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusName").Get<List<string>>();
            StaticAppSettings.TravellingStatusName = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusName").Get<List<string>>();
            StaticAppSettings.UnassignedStatusName = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusName").Get<List<string>>();
            StaticAppSettings.CanUnassignStatusName = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusName").Get<List<string>>();
            StaticAppSettings.DispatchedSubStatusNames = Configuration.GetSection("SubStatusNames:DispatchedNames").Get<List<string>>();
            StaticAppSettings.ConnectionString = Configuration.GetSection("ConnectionString").Get<string>();
            StaticAppSettings.ExceedTimeHours = Configuration.GetSection("OrderSettingsColumns:ExceedTimeHours").Get<string>();
            StaticAppSettings.ArchiveAfterDays = Configuration.GetSection("OrderSettingsColumns:ArchiveAfterDays").Get<string>();
            StaticAppSettings.InitialStatus = Configuration.GetSection("StatusControl:InitialStatus").Get<KeyValue>();
            StaticAppSettings.InitialSubStatus = Configuration.GetSection("StatusControl:InitialSubStatus").Get<KeyValue>();
            StaticAppSettings.DispatchedStatus = Configuration.GetSection("StatusControl:DispatchedStatus").Get<KeyValue>();
            StaticAppSettings.OnHoldStatus = Configuration.GetSection("StatusControl:OnHoldStatus").Get<KeyValue>();
            StaticAppSettings.OnTravelSubStatus = Configuration.GetSection("OrderActionsPrerequisite:OnTravelSubStatus").Get<KeyValue>();
            StaticAppSettings.SMSContent = Configuration.GetSection("SMSContent").Get<string>();
            StaticAppSettings.WorkingTypes = Configuration.GetSection("WorkingTypes").Get<List<string>>();
            StaticAppSettings.IdleHandling = Configuration.GetSection("IdleHandling").Get<List<KeyValue>>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            // Add framework services.
            services.AddDbContext<OrderDBContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.Order.EFCore.MSSQL")));


            services.AddAutoMapper(typeof(Startup));
            //Mapper.AssertConfigurationIsValid();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            //services.AddIdentityServer();
            //services.Configure<VehicleAppSettings>(Configuration);
            #region Data & Managers
            services.AddScoped<DbContext, OrderDBContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));

            services.AddScoped(typeof(IOrderObject), typeof(OrderObject));
            services.AddScoped(typeof(IOrderSetting), typeof(OrderSetting));
            services.AddScoped(typeof(IArchivedOrder), typeof(ArchivedOrder));
            services.AddScoped(typeof(IOrderAction), typeof(OrderAction));
            services.AddScoped(typeof(INotificationCenter), typeof(NotificationCenter));

            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IOrderSettingManager), typeof(OrderSettingManager));
            services.AddScoped(typeof(IArchivedOrderManager), typeof(ArchivedOrderManager));
            services.AddScoped(typeof(IOrderActionManager), typeof(OrderActionManager));
            services.AddScoped(typeof(INotificationCenterManager), typeof(NotificationCenterManager));
            services.AddScoped(typeof(ITeamMembersService), typeof(TeamMembersService));
            services.AddScoped(typeof(INotificationService), typeof(NotificationService));
            services.AddScoped(typeof(ISMSService), typeof(SMSService));
            services.AddScoped(typeof(IProblemService), typeof(ProblemService));
            services.AddScoped(typeof(IIdentityService), typeof(IdentityService));
            services.AddScoped(typeof(IForemanService), typeof(ForemanService));
            services.AddScoped(typeof(IEngineerService), typeof(EngineerService));
            services.AddScoped(typeof(IManagerService), typeof(ManagerService));
            services.AddScoped(typeof(ITechnicianService), typeof(TechnicianService));
            services.AddScoped(typeof(IDispatcherService), typeof(DispatcherService));
            services.AddScoped(typeof(ITeamService), typeof(TeamService));
            services.AddScoped(typeof(ISupervisorService), typeof(SupervisorService));
            services.AddScoped(typeof(IDispatcherSettingsService), typeof(DispatcherSettingsService));
            services.AddScoped(typeof(ITechnicianStateService), typeof(TechnicianStateService));
            services.AddScoped(typeof(IAPIHelper), typeof(APIHelper));
            services.AddScoped<ArchiveOrderFlow>();
            services.AddScoped<TimeExceedFlow>();

            services.AddHostedService<ArchiveOrderTask>();
            services.AddHostedService<TimeExccedTask>();
            #endregion

            //   services.AddDbContext<DataContext>(options =>
            //options.UseSqlServer(Configuration["ConnectionString"],
            // sqlOptions => sqlOptions.MigrationsAssembly("Sprintoo.UserManagement.EFCore.MSSQL")));

            //// Dns Query for consul - service discovery
            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Order API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                // Assign scope requirements to operations based on AuthorizeAttribute
                // options.OperationFilter<SecurityRequirementsOperationFilter>();
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseStaticFiles();
            // Autoregister using server.Features (does not work in reverse proxy mode)
            app.UseConsulRegisterService();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Order API V1");
            }); // URL: /swagger
        }
    }
}
