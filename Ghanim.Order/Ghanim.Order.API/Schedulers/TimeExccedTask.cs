﻿using Ghanim.Order.API.Schedulers.TaskFlow;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Schedulers
{
    public class TimeExccedTask : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;
        private TimeExceedFlow _timeExceedFlow;
        public TimeExccedTask(ILogger<TimeExccedTask> logger,
            IServiceProvider serviceProvider )
        {
            _logger = logger;
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                _timeExceedFlow = scope.ServiceProvider.GetRequiredService<TimeExceedFlow>();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(3600));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");
            _timeExceedFlow.TimeExceedChecker();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
