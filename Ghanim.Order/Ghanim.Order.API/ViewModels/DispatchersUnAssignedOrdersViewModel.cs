﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class DispatchersUnAssignedOrdersViewModel
    {
        public int DispatcherId { get; set; }
        public IEnumerable<OrderViewModel> Orders { get; set; }
    }
}
