﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class ChangeRankInTeamViewModel
    {
        public int TeamId { get; set; }
        public Dictionary<int,int> Orders { get; set; }
    }
}
