﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class AcceptenceViewModel
    {
        public int OrderId { get; set; }
        public bool AcceptenceFlag { get; set; }
        public Nullable<int> RejectionReasonId { get; set; }
        public string RejectionReason { get; set; }
    }
}
