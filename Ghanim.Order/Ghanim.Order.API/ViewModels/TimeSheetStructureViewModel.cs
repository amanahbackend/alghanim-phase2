﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TimeSheetStructureViewModel
    {
        public DateTime DayDate { get; set; }
        public string PFNo { get; set; }
        public string Name { get; set; }
        //public Sheet Times { get; set; }

        public string WorkType { get; set; }
        public TimeSpan Time { get; set; }

        //public TimeSpan ActualTime { get; set; }
        //public TimeSpan TravellingTime { get; set; }
        //public TimeSpan BreakTime { get; set; }
        //public TimeSpan IdleTime { get; set; }
        //public TimeSpan TotalHours { get; set; }
        //public TimeSpan OverTimeHours { get; set; }
    }

    public class Sheet
    {
        public string WorkType { get; set; }
        public double Time { get; set; }
    }
}
