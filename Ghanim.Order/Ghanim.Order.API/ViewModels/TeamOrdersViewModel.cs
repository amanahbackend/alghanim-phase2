﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TeamOrdersViewModel
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<OrderViewModel> Orders { get; set; }
    }
}
