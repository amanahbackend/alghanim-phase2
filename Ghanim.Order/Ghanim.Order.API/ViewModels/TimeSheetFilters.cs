﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TimeSheetFilters
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string CostCenter { get; set; }
        public string PFNo { get; set; }
    }
}
