﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class NotificationCenterViewModel  : BaseEntity
    {
        public string RecieverId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Data { get; set; }
        public string NotificationType { get; set; }
        public bool? IsRead { get; set; }

    }
}
