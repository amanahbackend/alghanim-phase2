﻿using CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class WorkingViewModel
    {
        public int TechnicianId { get; set; }
        public int TeamId { get; set; }
        public int OrderId { get; set; }
        public int RejectionReasonId { get; set; }
        public string RejectionReason { get; set; }
        public DateTime ActionDate { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public TechnicianState State { get; set; }
        public TimeSheetType? Type { get; set; }
        public int ?WorkingTypeId { get; set; }
        public string WorkingTypeName { get; set; }
    }
}
