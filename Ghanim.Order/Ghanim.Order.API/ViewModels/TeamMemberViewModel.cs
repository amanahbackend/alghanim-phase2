﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnums;

namespace Ghanim.Order.API.ViewModels
{
    public class TeamMemberViewModel : BaseEntity
    {
        public int TeamId { get; set; }
        public int MemberParentId { get; set; }
        public string MemberParentName { get; set; }
        public int Rank { get; set; }
        public int MemberType { get; set; }
        public string MemberTypeName { get; set; }
        public string PF { get; set; }
        public string UserId { get; set; }
        public string CostCenter { get; set; }
        public int? CostCenterId { get; set; }
    }
}
