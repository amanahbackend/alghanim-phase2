﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class TeamModeViewModel
    {
        public string Mode { get; set; }
    }
}
