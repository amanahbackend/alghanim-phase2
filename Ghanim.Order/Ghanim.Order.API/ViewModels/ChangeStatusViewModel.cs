﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class ChangeStatusViewModel
    {
        public int OrderId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int SubStatusId { get; set; }
        public string SubStatusName { get; set; }
    }
}
