﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.ViewModels
{
    public class SMSViewModel
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
    }
}
