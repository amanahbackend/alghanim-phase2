﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addorderactiontype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActionTypeId",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActionTypeName",
                table: "OrderActions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionTypeId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "ActionTypeName",
                table: "OrderActions");
        }
    }
}
