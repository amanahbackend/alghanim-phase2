﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class removedrivedatafromorderActiontable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DriverName",
                table: "OrderActions");

            migrationBuilder.DropColumn(
                name: "DriverPFNumber",
                table: "OrderActions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "OrderActions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DriverName",
                table: "OrderActions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverPFNumber",
                table: "OrderActions",
                nullable: true);
        }
    }
}
