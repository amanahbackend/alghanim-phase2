﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addassigninginfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AcceptanceFlag",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DispatcherId",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IsAccomplish",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RejectionReason",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SupervisorId",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Order",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptanceFlag",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "DispatcherId",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "IsAccomplish",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "RejectionReason",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Order");
        }
    }
}
