﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addarchivedordertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Order",
                table: "Order");

            migrationBuilder.RenameTable(
                name: "Order",
                newName: "Orders");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Orders",
                table: "Orders",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ArchivedOrders",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentUserId = table.Column<string>(nullable: true),
                    OrderId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false),
                    TypeName = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    StatusName = table.Column<string>(nullable: true),
                    SubStatusId = table.Column<int>(nullable: false),
                    SubStatusName = table.Column<string>(nullable: true),
                    ProblemId = table.Column<int>(nullable: false),
                    ProblemName = table.Column<string>(nullable: true),
                    PriorityId = table.Column<int>(nullable: false),
                    PriorityName = table.Column<string>(nullable: true),
                    CompanyCodeId = table.Column<int>(nullable: false),
                    CompanyCodeName = table.Column<string>(nullable: true),
                    DivisionId = table.Column<int>(nullable: false),
                    DivisionName = table.Column<string>(nullable: true),
                    ICAgentNote = table.Column<string>(nullable: true),
                    DispatcherNote = table.Column<string>(nullable: true),
                    CancellationReason = table.Column<string>(nullable: true),
                    GeneralNote = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CustomerCode = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    PhoneOne = table.Column<string>(nullable: true),
                    PhoneTwo = table.Column<string>(nullable: true),
                    PACI = table.Column<string>(nullable: true),
                    FunctionalLocation = table.Column<string>(nullable: true),
                    HouseKasima = table.Column<string>(nullable: true),
                    Floor = table.Column<string>(nullable: true),
                    AppartmentNo = table.Column<string>(nullable: true),
                    StreetId = table.Column<int>(nullable: false),
                    StreetName = table.Column<string>(nullable: true),
                    BlockId = table.Column<int>(nullable: false),
                    BlockName = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: false),
                    AreaName = table.Column<string>(nullable: true),
                    GovId = table.Column<int>(nullable: false),
                    GovName = table.Column<string>(nullable: true),
                    AddressNote = table.Column<string>(nullable: true),
                    Long = table.Column<decimal>(nullable: false),
                    Lat = table.Column<decimal>(nullable: false),
                    BuildingTypeId = table.Column<int>(nullable: false),
                    BuildingTypeName = table.Column<string>(nullable: true),
                    ContractCode = table.Column<string>(nullable: true),
                    ContractTypeId = table.Column<int>(nullable: false),
                    ContractTypeName = table.Column<string>(nullable: true),
                    ContractStartDate = table.Column<DateTime>(nullable: false),
                    ContractExpiryDate = table.Column<DateTime>(nullable: false),
                    InsertionDate = table.Column<DateTime>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    SupervisorId = table.Column<int>(nullable: false),
                    SupervisorName = table.Column<string>(nullable: true),
                    DispatcherId = table.Column<int>(nullable: false),
                    DispatcherName = table.Column<string>(nullable: true),
                    TeamId = table.Column<int>(nullable: false),
                    AcceptanceFlag = table.Column<int>(nullable: false),
                    RejectionReasonId = table.Column<int>(nullable: false),
                    RejectionReason = table.Column<string>(nullable: true),
                    IsAccomplish = table.Column<int>(nullable: false),
                    IsRepeatedCall = table.Column<bool>(nullable: false),
                    IsExceedTime = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArchivedOrders", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArchivedOrders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Orders",
                table: "Orders");

            migrationBuilder.RenameTable(
                name: "Orders",
                newName: "Order");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Order",
                table: "Order",
                column: "Id");
        }
    }
}
