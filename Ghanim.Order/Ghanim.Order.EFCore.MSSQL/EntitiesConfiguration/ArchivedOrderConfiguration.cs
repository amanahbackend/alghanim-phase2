﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    class ArchivedOrderConfiguration : BaseEntityTypeConfiguration<ArchivedOrder>
    {
        public void Configure(EntityTypeBuilder<ArchivedOrder> builder)
        {
            builder.Property(x => x.Lat).HasColumnType("decimal(18,6)").IsRequired(false);
            builder.Property(x => x.Long).HasColumnType("decimal(18,6)").IsRequired(false);
            builder.ToTable("ArchivedOrder");
            base.Configure(builder);
        }
    }
}