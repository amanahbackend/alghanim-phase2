﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderActionConfiguration : BaseEntityTypeConfiguration<OrderAction>
    {
        public void Configure(EntityTypeBuilder<OrderAction> builder)
        {
            builder.ToTable("OrderAction");
            base.Configure(builder);
        }
    }
}
