﻿using Ghanim.Order.EFCore.MSSQL.EntitiesConfiguration;
using Ghanim.Order.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Order.EFCore.MSSQL.Context
{
    public class OrderDBContext : DbContext
    {
        public OrderDBContext(DbContextOptions<OrderDBContext> options) : base(options)
        {
        }
        public DbSet<OrderObject> Orders { get; set; }
        public DbSet<OrderSetting> Settings { get; set; }
        public DbSet<ArchivedOrder> ArchivedOrders { get; set; }
        public DbSet<OrderAction> OrderActions { get; set; }
        public DbSet<NotificationCenter> NotificationCenter { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderSettingConfiguration());
            modelBuilder.ApplyConfiguration(new ArchivedOrderConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationCenterConfiguration());
            modelBuilder.ApplyConfiguration(new OrderActionConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
