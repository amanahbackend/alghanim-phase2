﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Location.API.Controllers
{
    [Route("api/[controller]")]
    public class HealthCheckController : Controller
    {
        public HealthCheckController()
        {
        }

        [HttpGet]
        public IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}
