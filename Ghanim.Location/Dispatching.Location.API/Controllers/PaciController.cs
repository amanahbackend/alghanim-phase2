﻿using AutoMapper;
using Dispatching.Location.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PACI;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;

namespace Dispatching.Location.API.Controllers
{
    [Route("api/[controller]")]
    public class PaciController : Controller
    {
        private IConfigurationRoot _configurationRoot;
        public PaciController(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
            PACIHelper.Intialization(
                PACIConfig.proxyUrl,
                PACIConfig.paciServiceUrl,
                PACIConfig.paciNumberFieldName,
                PACIConfig.blockServiceUrl,
                PACIConfig.blockNameFieldNameBlockService,
                PACIConfig.nhoodNameFieldName,
                PACIConfig.streetServiceUrl,
                PACIConfig.blockNameFieldNameStreetService,
                PACIConfig.streetNameFieldName);
        }

        [HttpGet]
        [Route("GetLocationByPaci")]
        public IActionResult GetLocationByPaci(string paciNumber)
        {
            var paciData = new PACIHelper.PACIInfo();
            var round = 1;
            while (round < 3 && paciData.Latitude == null && paciData.Longtiude == null)
            {
                paciData = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber));
                round++;
            }

            var result = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber));
            var locationModel = Mapper.Map<PACIHelper.PACIInfo, LocationViewModel>(result);
            return Ok(locationModel);
        }

        [HttpGet]
        [Route("GetAllGovernorates")]
        public IActionResult GetAllGovernorates()
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:GovernorateService:ServiceUrl"];

            var result = PACIHelper.GetAllGovernorates(proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAreas")]
        public IActionResult GetAreas(int? govId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:AreaService:ServiceUrl"];
            string govIdfieldName = _configurationRoot["Location:AreaService:GovernorateIdFieldName"];
            var result = PACIHelper.GetAreas(govId, govIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBlocks")]
        public IActionResult GetBlocks(int? areaId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string areaIdfieldName = _configurationRoot["Location:BlockService:AreaIdFieldName"];
            var result = PACIHelper.GetBlocks(areaId, areaIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string govIdFieldName = _configurationRoot["Location:StreetService:GovIdFieldName"];
            string areaIdFieldName = _configurationRoot["Location:StreetService:AreaIdFieldName"];
            string blockNameFieldName = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            var result = PACIHelper.GetStreets(govId, govIdFieldName, areaId, areaIdFieldName,
                                                blockName, blockNameFieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet("GetStreetCoors")]
        public ProcessResultViewModel<Point> GetStreetCoors([FromQuery]string streetName, string blockName)
        {
            try
            {
                string proxyUrl = _configurationRoot["Location:ProxyUrl"];
                string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
                string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
                string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

                var result = PACIHelper.GetStreetPACItModel(streetName, blockName, proxyUrl,
                                            serviceUrl, streetFieldName, blockNameFieldNameStreetService);
                var point = new Point();
                if (result != null)
                {
                    point.Latitude = (double)result.features[0].attributes.CENTROID_Y;
                    point.Longitude = (double)result.features[0].attributes.CENTROID_X;
                }
                return ProcessResultViewModelHelper.Succedded(point);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<Point>(null, ex.Message);
            }
        }
    }
}
