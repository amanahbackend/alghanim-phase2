﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface IOrderSubStatus : IBaseEntity
    {
        string Name { get; set; }
        string Description { get; set; }
        string Code { get; set; }
        int StatusId { get; set; }
        int StatusName { get; set; }
    }
}
