﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface IShift : IBaseEntity
    {
        string Name { get; set; }
        DateTime FromTime { get; set; }
        DateTime ToTime { get; set; }
    }
}
