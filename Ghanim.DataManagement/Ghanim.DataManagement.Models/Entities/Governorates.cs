﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Governorates : BaseEntity , IGovernorates
    {
        public string Name { get; set; }
        public int Gov_No { get; set; }
        public List<Areas> Areas {get; set; }
    }
}
