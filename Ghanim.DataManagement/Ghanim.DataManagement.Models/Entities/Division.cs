﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
   public class Division :BaseEntity , IDivision
    {
      public  string Name { get; set; }
      public  string code { get; set; }
    }
}
