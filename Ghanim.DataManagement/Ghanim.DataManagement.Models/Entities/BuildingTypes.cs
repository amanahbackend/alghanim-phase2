﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
   public class BuildingTypes :BaseEntity , IBuildingTypes
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
