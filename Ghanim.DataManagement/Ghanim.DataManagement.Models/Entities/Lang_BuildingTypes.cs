﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Lang_BuildingTypes : BaseEntity, ILang_BuildingTypes
    {
        public int FK_BuildingTypes_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public BuildingTypes BuildingTypes { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
