﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Areas : BaseEntity, IAreas
    {
        public int Area_No { get; set; }
        public string Name { get; set; }
        public int? Fk_Governorate_Id { get; set; }
        [ForeignKey("Fk_Governorate_Id")]
        public Governorates Governorates { get; set; }
    }
}
