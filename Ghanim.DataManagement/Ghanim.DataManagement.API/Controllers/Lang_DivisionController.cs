﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_DivisionController : BaseController<ILang_DivisionManager, Lang_Division, Lang_DivisionViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_DivisionManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_DivisionController(IServiceProvider serviceprovider, ILang_DivisionManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IDivisionManager Divisionmanager
        {
            get
            {
                return _serviceprovider.GetService<IDivisionManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByDivisionId/{DivisionId}")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesByDivisionId([FromRoute]int DivisionId)
        {
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var DivisionRes = Divisionmanager.Get(DivisionId);
            var entityResult = manager.GetAllLanguagesByDivisionId(DivisionId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = DivisionRes.Data.code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var DivisionRes = Divisionmanager.GetAll();
            foreach (var Division in DivisionRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByDivisionId(Division.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = Division.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Division.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = Division.code, languagesDictionaries = languagesDictionary, Id = Division.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Division> lstModel)
        {
            var entityResult = manager.UpdateByDivision(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]CodeWithAllLanguagesViewModel Model)
        {
            List<Lang_Division> lstModel = new List<Lang_Division>();
            var DivisionRes = Divisionmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                lstModel.Add(new Lang_Division { FK_Division_ID = DivisionRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByDivision(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
