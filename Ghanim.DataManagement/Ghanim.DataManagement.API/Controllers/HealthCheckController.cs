﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class HealthCheckController : Controller
    {
        public HealthCheckController()
        {
        }

        [HttpGet(""), MapToApiVersion("1.0")]
        [HttpHead("")]
        public IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}