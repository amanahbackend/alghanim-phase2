﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderProblemController : BaseController<IOrderProblemManager, OrderProblem, OrderProblemViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderProblemManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public OrderProblemController(IOrderProblemManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
    }
}
