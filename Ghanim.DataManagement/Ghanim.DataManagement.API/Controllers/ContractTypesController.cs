﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class ContractTypesController : BaseController<IContractTypesManager, ContractTypes, ContractTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        IContractTypesManager manager;
        IProcessResultMapper processResultMapper;
        public ContractTypesController(IServiceProvider serviceprovider, IContractTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_ContractTypesManager Lang_ContractTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_ContractTypesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetContractTypesByLanguage")]
        public ProcessResultViewModel<ContractTypesViewModel> GetContractTypesByLanguage([FromQuery]int ContractTypeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_ContractTypesRes = Lang_ContractTypesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(ContractTypeId);
            entityResult.Data.Name = Lang_ContractTypesRes.Data.Name;
            var result = processResultMapper.Map<ContractTypes, ContractTypesViewModel>(entityResult);
            return result;
        }


        //[HttpGet]
        //[Route("GetContractTypesWithAllLanguages/{ContractTypeId}")]
        //public ProcessResultViewModel<List<ContractTypesViewModel>> GetContractTypeWithAllLanguages([FromRoute]int ContractTypeId)
        //{
        //    ProcessResult<List<ContractTypes>> entityResult = null;
        //    var Lang_ContractTypesRes = Lang_ContractTypesmanager.GetAll().Data.FindAll(x => x.FK_ContractTypes_ID == ContractTypeId);
        //    foreach (var item in Lang_ContractTypesRes)
        //    {
        //        var ContractTypesRes = manager.Get(x => x.Id == item.FK_ContractTypes_ID);
        //        ContractTypesRes.Data.Name = item.Name;
        //        entityResult.Data.Add(ContractTypesRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<ContractTypes>, List<ContractTypesViewModel>>(entityResult);
        //    return result;
        //}
    }
}
