﻿using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_CostCenterController : BaseController<ILang_CostCenterManager, Lang_CostCenter, Lang_CostCenterViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_CostCenterManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_CostCenterController(IServiceProvider serviceprovider, ILang_CostCenterManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ICostCenterManager CostCentermanager
        {
            get
            {
                return _serviceprovider.GetService<ICostCenterManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByCostCenterId/{CostCenterId}")]
        public ProcessResultViewModel<List<LanguagesDictionaryViewModel>> GetAllLanguagesByCostCenterId([FromRoute]int CostCenterId)
        {
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var CostCenterRes = CostCentermanager.Get(CostCenterId);
            var entityResult = manager.GetAllLanguagesByCostCenterId(CostCenterId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionaryViewModel>>(languagesDictionary);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_CostCenter> lstModel)
        {
            var entityResult = manager.UpdateByCostCenter(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
