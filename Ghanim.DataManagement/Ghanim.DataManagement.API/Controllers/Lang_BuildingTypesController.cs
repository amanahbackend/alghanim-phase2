﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_BuildingTypesController : BaseController<ILang_BuildingTypesManager, Lang_BuildingTypes, Lang_BuildingTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_BuildingTypesManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_BuildingTypesController(IServiceProvider serviceprovider, ILang_BuildingTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IBuildingTypesManager BuildingTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<IBuildingTypesManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByBuildingTypesId/{BuildingTypesId}")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesByBuildingTypesId([FromRoute]int BuildingTypesId)
        {
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var BuildingTypesRes = BuildingTypesmanager.Get(BuildingTypesId);
            var entityResult = manager.GetAllLanguagesByBuildingTypesId(BuildingTypesId);
            foreach (var item in entityResult.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = BuildingTypesRes.Data.Code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);

            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var BuildingTypesRes = BuildingTypesmanager.GetAll();
            foreach (var BuildingTypes in BuildingTypesRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByBuildingTypesId(BuildingTypes.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = BuildingTypes.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = BuildingTypes.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = BuildingTypes.Code, languagesDictionaries = languagesDictionary, Id = BuildingTypes.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_BuildingTypes> lstModel)
        {
            var entityResult = manager.UpdateByBuildingTypes(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]CodeWithAllLanguagesViewModel Model)
        {
            List<Lang_BuildingTypes> lstModel = new List<Lang_BuildingTypes>();
            var BuildingTypesRes = BuildingTypesmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                lstModel.Add(new Lang_BuildingTypes { FK_BuildingTypes_ID = BuildingTypesRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByBuildingTypes(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
