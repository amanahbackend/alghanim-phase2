﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderSubStatusController : BaseController<IOrderSubStatusManager, OrderSubStatus, OrderSubStatusViewModel>
    {
        IOrderSubStatusManager manager;
        public OrderSubStatusController(IOrderSubStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
        }

        [HttpGet]
        [Route("GetByStatusId")]
        public ProcessResult<List<OrderSubStatus>> GetByStatusId([FromQuery] int id)
        {
            return manager.GetByStatusId(id);
        }

    }
}