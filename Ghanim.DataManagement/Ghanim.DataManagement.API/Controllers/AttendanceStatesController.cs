﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;


namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class AttendanceStatesController : BaseController<IAttendanceStatesManager, AttendanceStates, AttendanceStatesViewModel>
    {
        IServiceProvider _serviceprovider;
        IAttendanceStatesManager manager;
        IProcessResultMapper processResultMapper;
        public AttendanceStatesController(IServiceProvider serviceprovider, IAttendanceStatesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_AttendanceStatesManager Lang_AttendanceStatesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AttendanceStatesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAttendanceStatesByLanguage")]
        public ProcessResultViewModel<AttendanceStatesViewModel> GetAttendanceStatesByLanguage([FromQuery]int AttendanceStatesId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AttendanceStatesRes = Lang_AttendanceStatesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AttendanceStatesId);
            entityResult.Data.Name = Lang_AttendanceStatesRes.Data.Name;
            var result = processResultMapper.Map<AttendanceStates, AttendanceStatesViewModel>(entityResult);
            return result;
        }

        //[HttpGet]
        //[Route("GetAttendanceStatesWithAllLanguages/{AttendanceStatesId}")]
        //public ProcessResultViewModel<List<AttendanceStatesViewModel>> GetAttendanceStatesWithAllLanguages([FromRoute]int AttendanceStatesId)
        //{
        //    ProcessResult<List<AttendanceStates>> entityResult = null;
        //    var Lang_AttendanceStatesRes = Lang_AttendanceStatesmanager.GetAll().Data.FindAll(x => x.FK_AttendanceStates_ID == AttendanceStatesId);
        //    foreach (var item in Lang_AttendanceStatesRes)
        //    {
        //        var AttendanceStatesRes = manager.Get(x => x.Id == item.FK_AttendanceStates_ID);
        //        AttendanceStatesRes.Data.Name = item.Name;
        //        entityResult.Data.Add(AttendanceStatesRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<AttendanceStates>, List<AttendanceStatesViewModel>>(entityResult);
        //    return result;
        //}

    }
}
