﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class AreasController : BaseController<IAreasManager, Areas, AreasViewModel>
    {
        IServiceProvider _serviceprovider;
        IAreasManager manager;
        IProcessResultMapper processResultMapper;
        public AreasController(IServiceProvider serviceprovider, IAreasManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_AreasManager Lang_Areasmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AreasManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAreaByLanguage")]
        public ProcessResultViewModel<AreasViewModel> GetAreaByLanguage([FromQuery]int AreaId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AreasRes = Lang_Areasmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AreaId);
            entityResult.Data.Name = Lang_AreasRes.Data.Name;
            var result = processResultMapper.Map<Areas,AreasViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetAreasByGovId")]
        public ProcessResultViewModel<List<AreasViewModel>> GetAreasByGovId([FromQuery]int GovId)
        {
            var entityResult = manager.GetAreasByGovId(GovId);
            var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
            return result;
        }

        //[HttpGet]
        //[Route("GetAreaWithAllLanguages/{AreaId}")]
        //public ProcessResultViewModel<List<AreasViewModel>> GetAreaWithAllLanguages([FromRoute]int AreaId)
        //{
        //    ProcessResult<List<Areas>> entityResult = null;
        //    var Lang_AreasRes = Lang_Areasmanager.GetAll().Data.FindAll(x=>x.FK_Area_ID==AreaId);
        //    foreach (var item in Lang_AreasRes)
        //    {
        //        var AreasRes = manager.Get(x => x.Id == item.FK_Area_ID);
        //        AreasRes.Data.Name = item.Name;
        //        entityResult.Data.Add(AreasRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
        //    return result;
        //}


    }
}
