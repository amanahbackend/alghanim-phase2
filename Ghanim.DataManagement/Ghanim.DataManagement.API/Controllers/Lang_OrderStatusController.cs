﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class Lang_OrderStatusController : BaseController<ILang_OrderStatusManager, Lang_OrderStatus, Lang_OrderStatusViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_OrderStatusManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_OrderStatusController(IServiceProvider serviceprovider, ILang_OrderStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }  
        private IOrderStatusManager OrderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderStatusManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByOrderStatusId/{OrderStatusId}")]
        public ProcessResultViewModel<List<LanguagesDictionaryViewModel>> GetAllLanguagesByOrderStatusId([FromRoute]int OrderStatusId)
        {
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var OrderStatusRes = OrderStatusmanager.Get(OrderStatusId);
            var entityResult = manager.GetAllLanguagesByOrderStatusId(OrderStatusId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionaryViewModel>>(languagesDictionary);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<NullWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();

            List<NullWithAllLanguagesViewModel> nullWithAllLanguages = new List<NullWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var OrderStatusRes = OrderStatusmanager.GetAll();
            foreach (var OrderStatus in OrderStatusRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByOrderStatusId(OrderStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = OrderStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = OrderStatus.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                nullWithAllLanguages.Add(new NullWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary,Id= OrderStatus.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<NullWithAllLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_OrderStatus> lstModel)
        {
            var entityResult = manager.UpdateByOrderStatus(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]NullWithAllLanguagesViewModel Model)
        {
            List<Lang_OrderStatus> lstModel = new List<Lang_OrderStatus>();
            var OrderStatusRes = OrderStatusmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                lstModel.Add(new Lang_OrderStatus { FK_OrderStatus_ID = OrderStatusRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
            }
            var entityResult = manager.UpdateByOrderStatus(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}