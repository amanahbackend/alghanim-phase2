﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class BuildingTypesController : BaseController<IBuildingTypesManager, BuildingTypes, BuildingTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        IBuildingTypesManager manager;
        IProcessResultMapper processResultMapper;
        public BuildingTypesController(IServiceProvider serviceprovider, IBuildingTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_BuildingTypesManager Lang_BuildingTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_BuildingTypesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetBuildingTypeByLanguage")]
        public ProcessResultViewModel<BuildingTypesViewModel> GetBuildingTypeByLanguage([FromQuery]int BuildingTypeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_BuildingTypesRes = Lang_BuildingTypesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(BuildingTypeId);
            entityResult.Data.Name = Lang_BuildingTypesRes.Data.Name;
            var result = processResultMapper.Map<BuildingTypes, BuildingTypesViewModel>(entityResult);
            return result;
        }


        //[HttpGet]
        //[Route("GetBuildingTypesWithAllLanguages/{BuildingTypeId}")]
        //public ProcessResultViewModel<List<BuildingTypesViewModel>> GetBuildingTypesWithAllLanguages([FromRoute]int BuildingTypeId)
        //{
        //    ProcessResult<List<BuildingTypes>> entityResult = null;
        //    var Lang_BuildingTypesRes = Lang_BuildingTypesmanager.GetAll().Data.FindAll(x => x.FK_BuildingTypes_ID == BuildingTypeId);
        //    foreach (var item in Lang_BuildingTypesRes)
        //    {
        //        var BuildingTypesRes = manager.Get(x => x.Id == item.FK_BuildingTypes_ID);
        //        BuildingTypesRes.Data.Name = item.Name;
        //        entityResult.Data.Add(BuildingTypesRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<BuildingTypes>, List<BuildingTypesViewModel>>(entityResult);
        //    return result;
        //}
    }
}
