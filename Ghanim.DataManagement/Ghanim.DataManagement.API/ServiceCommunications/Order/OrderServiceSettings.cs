﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ServiceCommunications.Order
{
    public class OrderServiceSettings : DefaultHttpClientSettings
    {
        public string ServiceName { get; set; }
        public string AddOrder { get; set; }
        public string AddOrders { get; set; }
        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}