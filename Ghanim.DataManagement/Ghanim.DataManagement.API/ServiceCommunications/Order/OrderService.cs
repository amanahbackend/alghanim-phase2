﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.DataManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.Order
{
    public class OrderService : DefaultHttpClientCrud<OrderServiceSettings, OrderViewModel, OrderViewModel>,
       IOrderService
    {
        OrderServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public OrderService(IOptions<OrderServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<OrderViewModel>> AddOrder(OrderViewModel order)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddOrder;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<OrderViewModel,OrderViewModel>(url, order);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<OrderViewModel>>> AddOrders(List<OrderViewModel> orders)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddOrders;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<List<OrderViewModel>, List<OrderViewModel>>(url, orders);
            }
            return null;
        }
    }
}
