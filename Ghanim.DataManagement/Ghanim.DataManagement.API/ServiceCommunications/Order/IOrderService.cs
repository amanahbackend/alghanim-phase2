﻿using DispatchProduct.HttpClient;
using Ghanim.DataManagement.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.Order
{
    public interface IOrderService : IDefaultHttpClientCrud<OrderServiceSettings, OrderViewModel, OrderViewModel>
    {
        Task<ProcessResultViewModel<OrderViewModel>> AddOrder(OrderViewModel order);
        Task<ProcessResultViewModel<List<OrderViewModel>>> AddOrders(List<OrderViewModel> orders);
    }
}
