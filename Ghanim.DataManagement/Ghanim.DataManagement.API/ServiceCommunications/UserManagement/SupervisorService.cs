﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.DataManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public class SupervisorService : DefaultHttpClientCrud<UserManagementServiceSettings, SupervisorViewModel, SupervisorViewModel>,
       ISupervisorService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public SupervisorService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<SupervisorViewModel>> GetSupervisorByDivisionId(int divisonId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetSupervisorByDivisionId;
                var url = $"{baseUrl}/{requestedAction}/{divisonId}";
                return await GetByUriCustomized<SupervisorViewModel>(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetSupervisorsByDivisionId(int divisonId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetSupervisorsByDivisionId;
                var url = $"{baseUrl}/{requestedAction}/{divisonId}";
                return await GetByUriCustomized<List<SupervisorViewModel>>(url);
            }
            return null;
        }
    }
}
