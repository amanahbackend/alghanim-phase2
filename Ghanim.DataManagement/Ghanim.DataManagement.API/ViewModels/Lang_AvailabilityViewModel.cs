﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class Lang_AvailabilityViewModel : RepoistryBaseEntity
    {
        public int FK_Availability_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public Availability Availability { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
