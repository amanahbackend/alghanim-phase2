﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class Lang_CostCenterViewModel : RepoistryBaseEntity
    {
        public int FK_CostCenter_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public CostCenter CostCenter { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
