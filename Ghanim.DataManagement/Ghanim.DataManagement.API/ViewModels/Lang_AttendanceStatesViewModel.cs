﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class Lang_AttendanceStatesViewModel : RepoistryBaseEntity
    {
        public int FK_AttendanceStates_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public AttendanceStates AttendanceStates { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
