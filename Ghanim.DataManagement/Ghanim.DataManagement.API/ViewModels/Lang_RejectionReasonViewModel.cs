﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class Lang_RejectionReasonViewModel : BaseEntity
    {
        public int FK_RejectionReason_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public RejectionReasonViewModel RejectionReason { get; set; }
        public SupportedLanguagesViewModel SupportedLanguages { get; set; }
    }
}
