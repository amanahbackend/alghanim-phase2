﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API.ViewModels
{
    public class AreaWithAllLanguagesViewModel : RepoistryBaseEntity
    {
        public int Area_No { get; set; }
        public List<LanguagesDictionaryViewModel> languagesDictionaries { get; set; }
    }
}
