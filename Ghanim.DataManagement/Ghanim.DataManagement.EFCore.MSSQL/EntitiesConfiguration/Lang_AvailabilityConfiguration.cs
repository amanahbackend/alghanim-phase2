﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_AvailabilityConfiguration : BaseEntityTypeConfiguration<Lang_Availability>, IEntityTypeConfiguration<Lang_Availability>
    {
        public override void Configure(EntityTypeBuilder<Lang_Availability> Lang_AvailabilityConfiguration)
        {
            base.Configure(Lang_AvailabilityConfiguration);

            Lang_AvailabilityConfiguration.ToTable("Lang_Availability");
            Lang_AvailabilityConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_AvailabilityConfiguration.Property(o => o.Name).IsRequired();
            Lang_AvailabilityConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
            Lang_AvailabilityConfiguration.Property(o => o.FK_Availability_ID).IsRequired();
        }
    }
}
