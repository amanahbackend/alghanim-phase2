﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class BuildingTypesConfiguration : BaseEntityTypeConfiguration<BuildingTypes>, IEntityTypeConfiguration<BuildingTypes>
    {
        public override void Configure(EntityTypeBuilder<BuildingTypes> BuildingTypesConfiguration)
        {
            base.Configure(BuildingTypesConfiguration);

            BuildingTypesConfiguration.ToTable("BuildingTypes");
            BuildingTypesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            BuildingTypesConfiguration.Property(o => o.Code).IsRequired();
            BuildingTypesConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
