﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class CompanyCodeConfiguration : BaseEntityTypeConfiguration<CompanyCode>, IEntityTypeConfiguration<CompanyCode>
   {
        public override void Configure(EntityTypeBuilder<CompanyCode> CompanyCodeConfiguration)
        {
            base.Configure(CompanyCodeConfiguration);

            CompanyCodeConfiguration.ToTable("CompanyCode");
            CompanyCodeConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CompanyCodeConfiguration.Property(o => o.Code).IsRequired();
            CompanyCodeConfiguration.Property(o => o.Name).IsRequired();
        }
   }
}
