﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_CompanyCodeConfiguration : BaseEntityTypeConfiguration<Lang_CompanyCode>, IEntityTypeConfiguration<Lang_CompanyCode>
    {
        public override void Configure(EntityTypeBuilder<Lang_CompanyCode> Lang_CompanyCodeConfiguration)
        {
            base.Configure(Lang_CompanyCodeConfiguration);

            Lang_CompanyCodeConfiguration.ToTable("Lang_CompanyCode");
            Lang_CompanyCodeConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_CompanyCodeConfiguration.Property(o => o.Name).IsRequired();
            Lang_CompanyCodeConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
            Lang_CompanyCodeConfiguration.Property(o => o.FK_CompanyCode_ID).IsRequired();
        }
    }
}
