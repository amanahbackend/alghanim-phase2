﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    class WorkingTypeConfiguration : BaseEntityTypeConfiguration<WorkingType>, IEntityTypeConfiguration<WorkingType>
    {
        public override void Configure(EntityTypeBuilder<WorkingType> builder)
        {
            builder.ToTable("WorkingType");
            base.Configure(builder);
        }
    }
}
