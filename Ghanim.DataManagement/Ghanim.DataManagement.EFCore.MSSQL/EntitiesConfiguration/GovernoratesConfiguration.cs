﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    class GovernoratesConfiguration : BaseEntityTypeConfiguration<Governorates>, IEntityTypeConfiguration<Governorates>
    {
        public override void Configure(EntityTypeBuilder<Governorates> GovernoratesConfiguration)
        {
            base.Configure(GovernoratesConfiguration);

            GovernoratesConfiguration.ToTable("Governorates");
            GovernoratesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            GovernoratesConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
