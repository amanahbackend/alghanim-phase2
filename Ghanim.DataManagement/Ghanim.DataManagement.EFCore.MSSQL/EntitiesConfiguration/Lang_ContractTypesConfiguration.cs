﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    class Lang_ContractTypesConfiguration : BaseEntityTypeConfiguration<Lang_ContractTypes>, IEntityTypeConfiguration<Lang_ContractTypes>
    {
        public override void Configure(EntityTypeBuilder<Lang_ContractTypes> Lang_ContractTypesConfiguration)
        {
            base.Configure(Lang_ContractTypesConfiguration);

            Lang_ContractTypesConfiguration.ToTable("Lang_ContractTypes");
            Lang_ContractTypesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_ContractTypesConfiguration.Property(o => o.Name).IsRequired();
            Lang_ContractTypesConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
            Lang_ContractTypesConfiguration.Property(o => o.FK_ContractTypes_ID).IsRequired();
        }
    }
}
