﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_OrderPriorityConfiguration : BaseEntityTypeConfiguration<Lang_OrderPriority>, IEntityTypeConfiguration<Lang_OrderPriority>
    {
        public override void Configure(EntityTypeBuilder<Lang_OrderPriority> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_OrderPriority");
        }
    }
}