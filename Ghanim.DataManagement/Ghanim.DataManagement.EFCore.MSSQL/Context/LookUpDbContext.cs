﻿
using Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.Context
{
   public class LookUpDbContext : DbContext
    {
        //public LookUpDbContext():base()
        //{
        //}
        public LookUpDbContext(DbContextOptions<LookUpDbContext> options)
            : base(options)
        {
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        //    optionsBuilder.UseSqlServer("Data Source=tcp:10.0.75.1,1433;Initial Catalog=Ghanim.DataManagement;User ID=sa;Password=123456");
        //}

        public DbSet<Areas> Areas { get; set; }
        public DbSet<AttendanceStates> AttendanceStates { get; set; }
        public DbSet<Availability> Availability { get; set; }
        public DbSet<BuildingTypes> BuildingTypes { get; set; }
        public DbSet<CompanyCode> CompanyCode { get; set; }
        public DbSet<ContractTypes> ContractTypes { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<Governorates> Governorates { get; set; }
        public DbSet<Shift> Shift { get; set; }
        public DbSet<Lang_Areas> Lang_Areas { get; set; }
        public DbSet<Lang_AttendanceStates> Lang_AttendanceStates { get; set; }
        public DbSet<CostCenter> CostCenter { get; set; }
        public DbSet<Lang_CostCenter> Lang_CostCenter { get; set; }
        public DbSet<Lang_Availability> Lang_Availability { get; set; }
        public DbSet<Lang_BuildingTypes> Lang_BuildingTypes { get; set; }
        public DbSet<Lang_CompanyCode> Lang_CompanyCode { get; set; }
        public DbSet<Lang_ContractTypes> Lang_ContractTypes { get; set; }
        public DbSet<Lang_Division> Lang_Division { get; set; }
        public DbSet<Lang_Governorates> Lang_Governorates { get; set; }
        public DbSet<Lang_Shift> Lang_Shift { get; set; }
        public DbSet<SupportedLanguages> SupportedLanguages { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Lang_OrderStatus> Lang_OrderStatuses { get; set; }
        public DbSet<OrderSubStatus> OrderSubStatuses{ get; set; }
        public DbSet<Lang_OrderSubStatus> Lang_OrderSubStatuses{ get; set; }
        public DbSet<OrderType> OrderTypes{ get; set; }
        public DbSet<Lang_OrderType> Lang_OrderTypes{ get; set; }
        public DbSet<OrderPriority> OrderPriorities{ get; set; }
        public DbSet<Lang_OrderPriority> Lang_OrderPriorities{ get; set; }
        public DbSet<OrderProblem> OrderProblems{ get; set; }
        public DbSet<Lang_OrderProblem> Lang_OrderProblems{ get; set; }
        public DbSet<OrderRowData> OrderRowData { get; set; }
        public DbSet<RejectionReason> RejectionReasons { get; set; }
        public DbSet<Lang_RejectionReason> Lang_RejectionReasons { get; set; }
        public DbSet<WorkingType> WorkingTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AreasConfiguration());
            modelBuilder.ApplyConfiguration(new AttendanceStatesConfiguration());
            modelBuilder.ApplyConfiguration(new AvailabilityConfiguration());
            modelBuilder.ApplyConfiguration(new BuildingTypesConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyCodeConfiguration());
            modelBuilder.ApplyConfiguration(new ContractTypesConfiguration());
            modelBuilder.ApplyConfiguration(new DivisionConfiguration());
            modelBuilder.ApplyConfiguration(new GovernoratesConfiguration());
            modelBuilder.ApplyConfiguration(new ShiftConfiguration());
            modelBuilder.ApplyConfiguration(new CostCenterConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_CostCenterConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_AreasConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_AttendanceStatesConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_AvailabilityConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_BuildingTypesConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_CompanyCodeConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_ContractTypesConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_DivisionConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_GovernoratesConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_ShiftConfiguration());
            modelBuilder.ApplyConfiguration(new SupportedLanguagesConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderProblemConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProblemConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderStatusConfiguraion());
            modelBuilder.ApplyConfiguration(new OrderStatusConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderSubStatusConfiguration());
            modelBuilder.ApplyConfiguration(new OrderSubStatusConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderPriorityConfiguration());
            modelBuilder.ApplyConfiguration(new OrderPriorityConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_OrderTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RejectionReasonConfiguration());
            modelBuilder.ApplyConfiguration(new Lang_RejectionReasonConfiguration());
            modelBuilder.ApplyConfiguration(new WorkingTypeConfiguration());
        }
    }
}
