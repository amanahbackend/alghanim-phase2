﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Area_No = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceStates",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Availability",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Availability", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BuildingTypes",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompanyCode",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyCode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractTypes",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CostCenter",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostCenter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Division",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    code = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Division", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Governorates",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Gov_No = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Governorates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_Areas",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_Area_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_AttendanceStates",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_AttendanceStates_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_AttendanceStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_Availability",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_Availability_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_Availability", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_BuildingTypes",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_BuildingTypes_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_BuildingTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_CompanyCode",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_CompanyCode_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_CompanyCode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_ContractTypes",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_ContractTypes_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_ContractTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_Division",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_Division_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_Division", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_Governorates",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_Governorates_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_Governorates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_Shift",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_Shift_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Day = table.Column<string>(nullable: false),
                    FromTime = table.Column<DateTime>(nullable: false),
                    ToTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_Shift", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shift",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<string>(nullable: false),
                    FromTime = table.Column<DateTime>(nullable: false),
                    ToTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shift", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SupportedLanguages",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportedLanguages", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.DropTable(
                name: "AttendanceStates");

            migrationBuilder.DropTable(
                name: "Availability");

            migrationBuilder.DropTable(
                name: "BuildingTypes");

            migrationBuilder.DropTable(
                name: "CompanyCode");

            migrationBuilder.DropTable(
                name: "ContractTypes");

            migrationBuilder.DropTable(
                name: "CostCenter");

            migrationBuilder.DropTable(
                name: "Division");

            migrationBuilder.DropTable(
                name: "Governorates");

            migrationBuilder.DropTable(
                name: "Lang_Areas");

            migrationBuilder.DropTable(
                name: "Lang_AttendanceStates");

            migrationBuilder.DropTable(
                name: "Lang_Availability");

            migrationBuilder.DropTable(
                name: "Lang_BuildingTypes");

            migrationBuilder.DropTable(
                name: "Lang_CompanyCode");

            migrationBuilder.DropTable(
                name: "Lang_ContractTypes");

            migrationBuilder.DropTable(
                name: "Lang_Division");

            migrationBuilder.DropTable(
                name: "Lang_Governorates");

            migrationBuilder.DropTable(
                name: "Lang_Shift");

            migrationBuilder.DropTable(
                name: "Shift");

            migrationBuilder.DropTable(
                name: "SupportedLanguages");
        }
    }
}
