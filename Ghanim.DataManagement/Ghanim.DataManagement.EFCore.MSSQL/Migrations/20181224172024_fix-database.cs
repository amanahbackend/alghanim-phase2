﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class fixdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.DropIndex(
                name: "IX_Lang_RejectionReason_RejectionReasonId",
                table: "Lang_RejectionReason");

            migrationBuilder.DropIndex(
                name: "IX_Lang_RejectionReason_SupportedLanguagesId",
                table: "Lang_RejectionReason");

            migrationBuilder.DropColumn(
                name: "Day",
                table: "Lang_Shift");

            migrationBuilder.DropColumn(
                name: "RejectionReasonId",
                table: "Lang_RejectionReason");

            migrationBuilder.DropColumn(
                name: "SupportedLanguagesId",
                table: "Lang_RejectionReason");

            migrationBuilder.RenameColumn(
                name: "Day",
                table: "Shift",
                newName: "Name");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Lang_Shift",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Fk_Governorate_Id",
                table: "Areas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Areas_Fk_Governorate_Id",
                table: "Areas",
                column: "Fk_Governorate_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_Governorates_Fk_Governorate_Id",
                table: "Areas",
                column: "Fk_Governorate_Id",
                principalTable: "Governorates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Lang_Shift");

            migrationBuilder.DropColumn(
                name: "Fk_Governorate_Id",
                table: "Areas");

           
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Shift",
                newName: "Day");

            migrationBuilder.AddColumn<string>(
                name: "Day",
                table: "Lang_Shift",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "RejectionReasonId",
                table: "Lang_RejectionReason",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupportedLanguagesId",
                table: "Lang_RejectionReason",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lang_RejectionReason_RejectionReasonId",
                table: "Lang_RejectionReason",
                column: "RejectionReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Lang_RejectionReason_SupportedLanguagesId",
                table: "Lang_RejectionReason",
                column: "SupportedLanguagesId");
            
        }
    }
}
