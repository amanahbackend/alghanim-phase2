﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class orderrowdatatableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrderRowData",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CustomerNo = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    PhoneOne = table.Column<string>(nullable: true),
                    PhoneTwo = table.Column<string>(nullable: true),
                    ContractNo = table.Column<string>(nullable: true),
                    ContractType = table.Column<string>(nullable: true),
                    ContractTypeDescription = table.Column<string>(nullable: true),
                    ContractDate = table.Column<DateTime>(nullable: true),
                    ContactExpiraion = table.Column<DateTime>(nullable: true),
                    FunctionalLocation = table.Column<string>(nullable: true),
                    PACI = table.Column<string>(nullable: true),
                    Governorate = table.Column<int>(nullable: true),
                    AreaCode = table.Column<int>(nullable: true),
                    AreaDescription = table.Column<string>(nullable: true),
                    Block = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    House = table.Column<string>(nullable: true),
                    Floor = table.Column<string>(nullable: true),
                    AppartmentNo = table.Column<int>(nullable: true),
                    AddressNote = table.Column<string>(nullable: true),
                    OrderNo = table.Column<string>(nullable: true),
                    OrderType = table.Column<string>(nullable: true),
                    OrderTypeDescription = table.Column<string>(nullable: true),
                    CompanyCode = table.Column<string>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: true),
                    Division = table.Column<int>(nullable: true),
                    DivisionDescription = table.Column<string>(nullable: true),
                    OrderPriority = table.Column<int>(nullable: true),
                    OrderPriorityDescription = table.Column<string>(nullable: true),
                    Problem = table.Column<string>(nullable: true),
                    ProblemDescription = table.Column<string>(nullable: true),
                    OrderStatus = table.Column<string>(nullable: true),
                    OrderNoteAgent = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderRowData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderRowData");
        }
    }
}
