﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class addGovAreaRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Fk_Governorate_Id",
                table: "Areas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Areas_Fk_Governorate_Id",
                table: "Areas",
                column: "Fk_Governorate_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_Governorates_Fk_Governorate_Id",
                table: "Areas",
                column: "Fk_Governorate_Id",
                principalTable: "Governorates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Areas_Governorates_Fk_Governorate_Id",
                table: "Areas");

            migrationBuilder.DropIndex(
                name: "IX_Areas_Fk_Governorate_Id",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "Fk_Governorate_Id",
                table: "Areas");
        }
    }
}
