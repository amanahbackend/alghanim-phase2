﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class CostCenterManager : Repository<CostCenter>, ICostCenterManager
    {
        public CostCenterManager(LookUpDbContext context)
            : base(context)
        {
        }
      public  ProcessResult<bool> costCenterIsExit(string name)
        {
            List<CostCenter> input = null;
            bool result = false;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Name == name).ToList();
                if (input!=null&&input.Count()>0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "costCenterIsExit");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(result, ex, (string)null, ProcessResultStatusCode.Failed, "costCenterIsExit");
            }
        }
    }
}
