﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_OrderStatusManager : Repository<Lang_OrderStatus>, ILang_OrderStatusManager
    {
        IServiceProvider _serviceprovider;

        public Lang_OrderStatusManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IOrderStatusManager orderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderStatusManager>();
            }
        }

        public ProcessResult<List<Lang_OrderStatus>> GetAllLanguagesByOrderStatusId(int OrderStatusId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == OrderStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderStatus { FK_OrderStatus_ID = OrderStatusId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderStatusId");
            }
        }

        public ProcessResult<bool> UpdateByOrderStatus(List<Lang_OrderStatus> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var orderstatusRes = orderStatusmanager.Get(item.FK_OrderStatus_ID);
                        if (orderstatusRes != null && orderstatusRes.Data != null)
                        {
                            orderstatusRes.Data.Name = item.Name;
                            var updateRes = orderStatusmanager.Update(orderstatusRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderStatus = GetByOrderStatusIdAndsupprotedLangId(item.FK_OrderStatus_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderStatus != null && prevLangOrderStatus.Data != null && prevLangOrderStatus.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
        }

        private ProcessResult<List<Lang_OrderStatus>> GetByOrderStatusIdAndsupprotedLangId(int OrderStatusId, int supprotedLangId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == OrderStatusId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByOrderStatusIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderStatusIdAndsupprotedLangId");
            }
        }
    }
}