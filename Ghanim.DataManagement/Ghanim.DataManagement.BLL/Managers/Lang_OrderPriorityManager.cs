﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_OrderPriorityManager : Repository<Lang_OrderPriority>, ILang_OrderPriorityManager
    {
        IServiceProvider _serviceprovider;
        public Lang_OrderPriorityManager(IServiceProvider serviceprovider, LookUpDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IOrderPriorityManager orderPriorityManager
        {
            get
            {
                return _serviceprovider.GetService<IOrderPriorityManager>();
            }
        }

        public ProcessResult<List<Lang_OrderPriority>> GetAllLanguagesByOrderPriorityId(int OrderPriorityId)
        {
            List<Lang_OrderPriority> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_OrderPriority_ID == OrderPriorityId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderPriority { FK_OrderPriority_ID = OrderPriorityId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderPriority>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderPriorityId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderPriority>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderPriorityId");
            }
        }

        public ProcessResult<bool> UpdateByOrderPriority(List<Lang_OrderPriority> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var orderPriorityRes = orderPriorityManager.Get(item.FK_OrderPriority_ID);
                        if (orderPriorityRes != null && orderPriorityRes.Data != null)
                        {
                            orderPriorityRes.Data.Name = item.Name;
                            var updateRes = orderPriorityManager.Update(orderPriorityRes.Data);
                        }
                    }
                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderPriority = GetByOrderPriorityIdAndsupprotedLangId(item.FK_OrderPriority_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderPriority != null && prevLangOrderPriority.Data != null && prevLangOrderPriority.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderPriority");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderPriority");
            }
        }

        private ProcessResult<List<Lang_OrderPriority>> GetByOrderPriorityIdAndsupprotedLangId(int OrderPriorityId, int supprotedLangId)
        {
            List<Lang_OrderPriority> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_OrderPriority_ID == OrderPriorityId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderPriority>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByOrderPriorityIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderPriority>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderPriorityIdAndsupprotedLangId");
            }
        }
    }
}
