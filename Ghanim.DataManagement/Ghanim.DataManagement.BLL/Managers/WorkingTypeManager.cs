﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class WorkingTypeManager : Repository<WorkingType>, IWorkingTypeManager
    {
        public WorkingTypeManager(LookUpDbContext context)
            : base(context)
        {
        }
    }
}
