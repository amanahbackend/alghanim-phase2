﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_ShiftManager : IRepository<Lang_Shift>
    {
        ProcessResult<List<Lang_Shift>> GetAllLanguagesByShiftId(int ShiftId);
        ProcessResult<bool> UpdateByShift(List<Lang_Shift> entities);
    }
}
