﻿using Ghanim.DataManagement.BLL.ExcelSettings;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface IReadOrderManager
    {
        ProcessResult<List<OrderObject>> Process(string path, UploadFile Uploadfile);
        void ReadFile();
    }
}