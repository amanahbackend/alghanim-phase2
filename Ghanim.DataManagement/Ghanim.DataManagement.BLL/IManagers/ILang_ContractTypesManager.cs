﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_ContractTypesManager : IRepository<Lang_ContractTypes>
    {
        ProcessResult<List<Lang_ContractTypes>> GetAllLanguagesByContractTypesId(int ContractTypesId);
        ProcessResult<bool> UpdateByContractTypes(List<Lang_ContractTypes> entities);
    }
}
