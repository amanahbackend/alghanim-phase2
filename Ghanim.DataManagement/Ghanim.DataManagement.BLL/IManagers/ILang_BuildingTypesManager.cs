﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_BuildingTypesManager : IRepository<Lang_BuildingTypes>
    {
        ProcessResult<List<Lang_BuildingTypes>> GetAllLanguagesByBuildingTypesId(int BuildingTypesId);
        ProcessResult<bool> UpdateByBuildingTypes(List<Lang_BuildingTypes> entities);
    }
}
