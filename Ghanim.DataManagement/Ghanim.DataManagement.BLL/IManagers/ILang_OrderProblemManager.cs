﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_OrderProblemManager : IRepository<Lang_OrderProblem>
    {
        ProcessResult<List<Lang_OrderProblem>> GetAllLanguagesByOrderProblemId(int OrderProblemId);
        ProcessResult<bool> UpdateByOrderProblem(List<Lang_OrderProblem> entities);
    }
}