﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface IOrderStatusManager : IRepository<OrderStatus>
    {
        ProcessResult<List<OrderStatus>> GetAllExclude(List<int> excludedStatuseIds);
    }
}
