﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_AvailabilityManager : IRepository<Lang_Availability>
    {
        ProcessResult<List<Lang_Availability>> GetAllLanguagesByAvailabilityId(int AvailabilityId);
        ProcessResult<bool> UpdateByAvailability(List<Lang_Availability> entities);
    }
}
