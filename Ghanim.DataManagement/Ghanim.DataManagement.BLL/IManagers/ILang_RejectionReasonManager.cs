﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_RejectionReasonManager : IRepository<Lang_RejectionReason>
    {
    }
}
