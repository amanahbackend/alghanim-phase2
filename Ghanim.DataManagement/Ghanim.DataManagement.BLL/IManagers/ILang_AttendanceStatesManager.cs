﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_AttendanceStatesManager : IRepository<Lang_AttendanceStates>
    {
        ProcessResult<List<Lang_AttendanceStates>> GetAllLanguagesByAttendanceStatesId(int AttendanceStatesId);
        ProcessResult<bool> UpdateByAttendanceStates(List<Lang_AttendanceStates> entities);
    }
}
